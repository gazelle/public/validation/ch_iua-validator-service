package net.ihe.gazelle.app.iti71validator.application;

import net.ihe.gazelle.app.iti71validator.business.*;
import net.ihe.gazelle.app.validationservice.adapter.reportmodel.*;

import net.ihe.gazelle.app.validationservice.adapter.reportmodel.Error;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

class RequestHTTPgetURLValidatorTest {
    private RequestHTTPGetURLValidator validator;

    @BeforeEach
    public void initializeTest() {
        validator = new RequestHTTPGetURLValidator();
        validator.setValidatorDescription(IUAURLValidatorTestData.getValidatorDescriptionForGetTest());
        validator.setUpExtendedValidation(validator.getValidatorDescription().isExtended());
    }

    @Test
    void testSplitFullUrl() {
        String[] parts = validator.splitStringAndGetParts(IUAURLValidatorTestData.URL1_FULL, "\\?");
        Assertions.assertEquals(2, parts.length);
        Assertions.assertEquals(IUAURLValidatorTestData.URL1_BASE, parts[0]);
        Assertions.assertEquals(IUAURLValidatorTestData.URL1_PARAMETERS, parts[1]);
    }

    @Test
    void testSplitParameters() {
        String[] parts = validator.splitStringAndGetParts(IUAURLValidatorTestData.URL2_PARAMETERS, "&");
        Assertions.assertEquals(9, parts.length);
        Assertions.assertEquals(IUAURLValidatorTestData.URL2_FIRST_PARAMETER, parts[0]);
        Assertions.assertEquals(IUAURLValidatorTestData.URL2_SECOND_PARAMETER, parts[1]);
    }

    @Test
    void testSplitParameterKeyValue() {
        String[] parts = validator.splitStringAndGetParts(IUAURLValidatorTestData.URL2_FIRST_PARAMETER, "=");
        Assertions.assertEquals(2, parts.length);
        Assertions.assertEquals(IUAURLValidatorTestData.URL2_FIRST_PARAMETER_KEY, parts[0]);
        Assertions.assertEquals(IUAURLValidatorTestData.URL2_FIRST_PARAMETER_VALUE, parts[1]);
    }

    @Test
    void testParameterValueFormatDefaultRegex() {
        validator.validateParameterValueFormat(IUAURLValidatorTestData.URL2_FIRST_PARAMETER_VALUE, IUAURLValidatorTestData.PARAMETER_RESPONSETYPE_DESCRIPTION);
        checkRaisedNotification(Note.class);
    }

    @Test
    void testParameterValueFormatRegex() {
        validator.validateParameterValueFormat(IUAURLValidatorTestData.PARAMETER_CLIENTID, IUAURLValidatorTestData.PARAMETER_CLIENTID_DESCRIPTION);
        checkRaisedNotification(Error.class);
    }

    @Test
    void koTestParameterValueFormatDefautRegex() {
        validator.validateParameterValueFormat(IUAURLValidatorTestData.KO_PARAMETER, IUAURLValidatorTestData.KO_PARAMETER_DESCRIPTION);
        checkRaisedNotification(Error.class);
    }

    @Test
    void testAllowedParameter() {
        IIUARequestParameter parameter = validator.getParameterIfAllowed(IUAURLValidatorTestData.PARAMETER_RESPONSETYPE);
        Assertions.assertNotNull(parameter);
        checkRaisedNotification(Note.class);
    }

    @Test
    void testNotAllowedParameter() {
        IIUARequestParameter parameter = validator.getParameterIfAllowed(IUAURLValidatorTestData.KO_PARAMETER);
        Assertions.assertNull(parameter);
        checkRaisedNotification(Error.class);
    }

    @Test
    void testAllRequiredParameterPresents() {
        RequestHTTPGetURLValidator requestHTTPGetURLValidator = new RequestHTTPGetURLValidator();
        requestHTTPGetURLValidator.setValidatorDescription(IUAURLValidatorTestData.getValidatorDescriptionForGetTest());
        requestHTTPGetURLValidator.setUsedParameters(IUAURLValidatorTestData.getUsedParameterCompleteList());
        requestHTTPGetURLValidator.checkAllRequiredParametersArePresent();
        Assertions.assertEquals(9, requestHTTPGetURLValidator.getNotifications().size());
        Assertions.assertEquals(Note.class, requestHTTPGetURLValidator.getNotifications().get(0).getClass());
    }

    @Test
    void testCorrectlyFormattedParameter() {
        validator.validateUrlParameters(IUAURLValidatorTestData.URL2_PARAMETERS);
        Assertions.assertEquals(35, validator.getNotifications().size());
    }

    @Test
    void testKeys() {
        validator.validateParameterKey(IUAURLValidatorTestData.URL2_FIRST_PARAMETER_KEY);
        final List<Object> notifications = validator.getNotifications();
        Assertions.assertEquals(1, notifications.size());
        Assertions.assertEquals(Note.class, notifications.get(0).getClass());

    }

    @Test
    void testBadKeys() {
        validator.validateParameterKey(IUAURLValidatorTestData.KO_PARAMETER);
        final List<Object> notifications = validator.getNotifications();
        Assertions.assertEquals(2, notifications.size());
        Assertions.assertEquals(Error.class, notifications.get(0).getClass());

    }

    //   http://baseurl.com/authorize?
    //   response_type=code&client_id=app-client-id&http%3A%2F%2Flocalhost%3A9000%2Fcallback&launch=xyz123&
    //   scope=launch+user%2F%2A.%2A+openid+fhirUser&state=98wrghuwuogerg97&aud=https%3A%2F%2Fehr%2Ffhir&
    //   code_challenge=ZmVjMmIwMWYyYTNjZWJiNTgyNTgxYzlmOGYyMWM0MWI3YmZhMjQ4YjU5MDc3Mzk4MDBmYTk0OThlNzZiNjAwMw&code_challenge_method=S256";
    @Test
    void testFullUrl() {
        validator.validateRequest(IUAURLValidatorTestData.getValidatorDescriptionForGetTest());
        final List<Object> notifications = validator.getNotifications();
        Assertions.assertNotSame(0, notifications.size());
    }

    @Test
    void testParameterList() {
        validator.validateUrlParameters(IUAURLValidatorTestData.URL2_PARAMETERS);
        final List<Object> notifications = validator.getNotifications();
        Assertions.assertNotSame(0, notifications.size());
    }

    @Test
    void testProtocolHttp() {
        String url = validator.checkProtocolAndReturnUrl(IUAURLValidatorTestData.HTTP_URL);
        Assertions.assertNotNull(url);
        Assertions.assertEquals(IUAURLValidatorTestData.URL_NO_PROTOCOL, url);
    }

    @Test
    void testProtocolHttps() {
        String url = validator.checkProtocolAndReturnUrl(IUAURLValidatorTestData.HTTPS_URL);
        Assertions.assertNotNull(url);
        Assertions.assertEquals(IUAURLValidatorTestData.URL_NO_PROTOCOL, url);
    }

    @Test
    void koTestProtocol() {
        String url = validator.checkProtocolAndReturnUrl(IUAURLValidatorTestData.URL_NO_PROTOCOL);
        Assertions.assertNull(url);
        checkRaisedNotification(Error.class);
    }

    @Test
    void testNotValidDomain() {
        boolean result = validator.validateBaseUrlParts(IUAURLValidatorTestData.LOCALHOST, 0);
        Assertions.assertFalse(result);
    }

    @Test
    void testValidatorUrlCorrectResource() {
        validator.validateBaseUrl(IUAURLValidatorTestData.BASE_URL_GET);
        int countErrors = 0;
        for (Object notification : validator.getNotifications()) {
            if (notification instanceof Error) {
                countErrors++;
            }
        }
        Assertions.assertEquals(0, countErrors);
    }

    @Test
    void testValidateRequest() {
        validator.validateRequest(IUAURLValidatorTestData.getValidatorDescriptionForGetTest());
        final List<Object> notifications = validator.getNotifications();
        Assertions.assertNotSame(0, notifications.size());
    }

    private void checkRaisedNotification(Class expectedClass) {
        Assertions.assertEquals(1, validator.getNotifications().size());
        Object notification = validator.getNotifications().get(0);
        if (notification instanceof Notification) {
            Assertions.assertEquals(expectedClass, notification.getClass(), ((Notification) notification).getDescription());
        }
    }
}
