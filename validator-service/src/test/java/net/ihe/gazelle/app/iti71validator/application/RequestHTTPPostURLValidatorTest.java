package net.ihe.gazelle.app.iti71validator.application;

import net.ihe.gazelle.app.iti71validator.business.IIUARequestParameter;
import net.ihe.gazelle.app.validationservice.adapter.reportmodel.Error;
import net.ihe.gazelle.app.validationservice.adapter.reportmodel.Note;
import net.ihe.gazelle.app.validationservice.adapter.reportmodel.Notification;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

class RequestHTTPPostURLValidatorTest {

    private RequestHTTPPostURLValidator validator;

    @BeforeEach
    public void initializeTest() {
        validator = new RequestHTTPPostURLValidator();
        validator.setValidatorDescription(IUAPostValidatorTestData.getValidatorDescriptionForPostTest());
    }

    @Test
    void testSplitFullUrl() {
        String[] parts = validator.splitStringAndGetParts(IUAPostValidatorTestData.URL1_FULL, "\\?");
        Assertions.assertEquals(2, parts.length);
        Assertions.assertEquals(IUAPostValidatorTestData.URL1_BASE, parts[0]);
        Assertions.assertEquals(IUAPostValidatorTestData.URL1_PARAMETERS, parts[1]);
    }

    @Test
    void testSplitParameters() {
        String[] parts = validator.splitStringAndGetParts(IUAPostValidatorTestData.URL1_PARAMETERS, "&");
        Assertions.assertEquals(4, parts.length);
        Assertions.assertEquals(IUAPostValidatorTestData.URL_FIRST_PARAMETER, parts[0]);
        Assertions.assertEquals(IUAPostValidatorTestData.URL_SECOND_PARAMETER, parts[1]);
    }

    @Test
    void testSplitParameterKeyValue() {
        String[] parts = validator.splitStringAndGetParts(IUAPostValidatorTestData.URL_FIRST_PARAMETER, "=");
        Assertions.assertEquals(2, parts.length);
        Assertions.assertEquals(IUAPostValidatorTestData.URL1_FIRST_PARAMETER_KEY, parts[0]);
        Assertions.assertEquals(IUAPostValidatorTestData.URL1_FIRST_PARAMETER_VALUE, parts[1]);
    }

    @Test
    void testParameterValueFormatDefaultRegex() {
        validator.validateParameterValueFormat(IUAPostValidatorTestData.URL1_FIRST_PARAMETER_VALUE, IUAPostValidatorTestData.PARAMETER_GRANTTYPE_DESCRIPTION);
        checkRaisedNotification(Note.class);
    }

    @Test
    void testParameterValueFormatRegex() {
        validator.validateParameterValueFormat(IUAPostValidatorTestData.PARAMETER_REDIRECTURI, IUAPostValidatorTestData.PARAMETER_REDIRECTURI_DESCRIPTION);
        checkRaisedNotification(Error.class);
    }

    @Test
    void koTestParameterValueFormatDefautRegex() {
        validator.validateParameterValueFormat(IUAPostValidatorTestData.KO_PARAMETER, IUAPostValidatorTestData.KO_PARAMETER_DESCRIPTION);
        checkRaisedNotification(Error.class);
    }

    @Test
    void testAllowedParameter() {
        IIUARequestParameter parameter = validator.getParameterIfAllowed(IUAPostValidatorTestData.PARAMETER_GRANTTYPE);
        Assertions.assertNotNull(parameter);
        checkRaisedNotification(Note.class);
    }

    @Test
    void testNotAllowedParameter() {
        IIUARequestParameter parameter = validator.getParameterIfAllowed(IUAPostValidatorTestData.KO_PARAMETER);
        Assertions.assertNull(parameter);
        checkRaisedNotification(Error.class);
    }

    @Test
    void testAllRequiredParameterPresents() {
        RequestHTTPPostURLValidator requestHTTPPostURLValidator = new RequestHTTPPostURLValidator();
        requestHTTPPostURLValidator.setValidatorDescription(IUAPostValidatorTestData.getValidatorDescriptionForPostTest());
        requestHTTPPostURLValidator.setUsedParameters(IUAPostValidatorTestData.getUsedParameterCompleteList());
        requestHTTPPostURLValidator.checkAllRequiredHeadersParametersArePresent();
        requestHTTPPostURLValidator.checkAllRequiredRequestParametersArePresent();
        Assertions.assertEquals(8, requestHTTPPostURLValidator.getNotifications().size());
        Assertions.assertEquals(Error.class, requestHTTPPostURLValidator.getNotifications().get(0).getClass());
    }

    @Test
    void testCorrectlyFormattedParameter() {
        validator.validateUrlParameters(IUAPostValidatorTestData.URL1_PARAMETERS);
        Assertions.assertEquals(19, validator.getNotifications().size());
    }

    @Test
    void testKeys() {
        validator.validateParameterKey(IUAPostValidatorTestData.URL1_FIRST_PARAMETER_KEY);
        final List<Object> notifications = validator.getNotifications();
        Assertions.assertEquals(1, notifications.size());
        Assertions.assertEquals(Note.class, notifications.get(0).getClass());

    }

    @Test
    void testBadKeys() {
        validator.validateParameterKey(IUAPostValidatorTestData.KO_PARAMETER);
        final List<Object> notifications = validator.getNotifications();
        Assertions.assertEquals(2, notifications.size());
        Assertions.assertEquals(Error.class, notifications.get(0).getClass());

    }

    //   http://baseurl.com/authorize?
    //   response_type=code&client_id=app-client-id&http%3A%2F%2Flocalhost%3A9000%2Fcallback&launch=xyz123&
    //   scope=launch+user%2F%2A.%2A+openid+fhirUser&state=98wrghuwuogerg97&aud=https%3A%2F%2Fehr%2Ffhir&
    //   code_challenge=ZmVjMmIwMWYyYTNjZWJiNTgyNTgxYzlmOGYyMWM0MWI3YmZhMjQ4YjU5MDc3Mzk4MDBmYTk0OThlNzZiNjAwMw&code_challenge_method=S256";
    @Test
    void testFullUrl() {
        validator.validateRequest(IUAPostValidatorTestData.getValidatorDescriptionForPostTest());
        final List<Object> notifications = validator.getNotifications();
        Assertions.assertNotSame(0, notifications.size());
    }

    @Test
    void testParameterList() {
        validator.validateUrlParameters(IUAPostValidatorTestData.URL1_PARAMETERS);
        final List<Object> notifications = validator.getNotifications();
        Assertions.assertNotSame(0, notifications.size());
    }

    @Test
    void testProtocolHttp() {
        String url = validator.checkProtocolAndReturnUrl(IUAPostValidatorTestData.HTTP_URL);
        Assertions.assertNotNull(url);
        Assertions.assertEquals(IUAPostValidatorTestData.URL_NO_PROTOCOL, url);
    }

    @Test
    void testProtocolHttps() {
        String url = validator.checkProtocolAndReturnUrl(IUAPostValidatorTestData.HTTPS_URL);
        Assertions.assertNotNull(url);
        Assertions.assertEquals(IUAPostValidatorTestData.URL_NO_PROTOCOL, url);
    }

    @Test
    void koTestProtocol() {
        String url = validator.checkProtocolAndReturnUrl(IUAPostValidatorTestData.URL_NO_PROTOCOL);
        Assertions.assertNull(url);
        checkRaisedNotification(Error.class);
    }

    @Test
    void testNotValidDomain() {
        boolean result = validator.validateBaseUrlParts(IUAPostValidatorTestData.LOCALHOST, 0);
        Assertions.assertFalse(result);
    }

    @Test
    void testValidatorUrlCorrectResource() {
        validator.validateBaseUrl(IUAPostValidatorTestData.BASE_URL_GET);
        int countErrors = 0;
        for (Object notification : validator.getNotifications()) {
            if (notification instanceof Error) {
                countErrors++;
            }
        }
        Assertions.assertEquals(0, countErrors);
    }

    @Test
    void testValidateRequest() {
        validator.validateRequest(IUAPostValidatorTestData.getValidatorDescriptionForPostTest());
        final List<Object> notifications = validator.getNotifications();
        Assertions.assertNotSame(0, notifications.size());
    }

    private void checkRaisedNotification(Class expectedClass) {
        Assertions.assertEquals(1, validator.getNotifications().size());
        Object notification = validator.getNotifications().get(0);
        if (notification instanceof Notification) {
            Assertions.assertEquals(expectedClass, notification.getClass(), ((Notification) notification).getDescription());
        }
    }

}
