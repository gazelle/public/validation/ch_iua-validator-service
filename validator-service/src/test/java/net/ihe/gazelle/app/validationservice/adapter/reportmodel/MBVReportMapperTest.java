package net.ihe.gazelle.app.validationservice.adapter.reportmodel;

import net.ihe.gazelle.modelapi.validationreportmodel.business.*;
import org.junit.jupiter.api.Test;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Tests for class [{@link MBVReportMapper}
 */
public class MBVReportMapperTest {

    private final static String UUID = "uuid";

    /**
     * Test the validation date time is correctly mapped.
     */
    @Test
    public void mapValidationDateTime() {
        TimeZone.setDefault(TimeZone.getTimeZone("UTC"));
        Date validationDateTime = new Date(Long.valueOf("1597842932511"));
        ValidationReport validationReport = new ValidationReport(UUID, new ValidationOverview("disclaimer", validationDateTime,
                "validationServiceName", "validationServiceVersion", "validatorID"));

        DetailedResult detailedResult = MBVReportMapper.map(validationReport);

        assertEquals("2020-08-19", detailedResult.getValidationResultsOverview().getValidationDate(), "Date shall be correctly mapped !");
        assertEquals("13:15:32", detailedResult.getValidationResultsOverview().getValidationTime(), "Validation Time shall be correctly mapped !");
    }

    /**
     * Test the mapping of the validationServiceName.
     */
    @Test
    public void mapValidationServiceName() {
        String validationServiceName = "validationServiceName";
        ValidationReport validationReport = new ValidationReport(UUID, new ValidationOverview("disclaimer",
                validationServiceName, "validationServiceVersion", "validatorID"));

        DetailedResult detailedResult = MBVReportMapper.map(validationReport);

        assertEquals(validationServiceName, detailedResult.getValidationResultsOverview().getValidationServiceName(),
                "Validation Service Name should be correctly mapped.");
    }


    /**
     * Test the mapping of the validationServiceVersion.
     */
    @Test
    public void mapValidationServiceVersion() {
        String validationServiceVersion = "validationServiceName";
        ValidationReport validationReport = new ValidationReport(UUID, new ValidationOverview("disclaimer",
                "validationServiceName", validationServiceVersion, "validatorID"));

        DetailedResult detailedResult = MBVReportMapper.map(validationReport);

        assertEquals(validationServiceVersion, detailedResult.getValidationResultsOverview().getValidationServiceVersion(),
                "Validation Service Version should be correctly mapped.");
    }

    /**
     * Test the mapping of Validation Test result when passed.
     */
    @Test
    public void mapValidationTestResultPassed() {
        ValidationOverview validationOverview = new ValidationOverview("disclaimer",
                "validationServiceName", "validationServiceVersion", "validatorID");
        validationOverview.setValidationOverallResult(ValidationTestResult.PASSED);
        ValidationReport validationReport = new ValidationReport(UUID, validationOverview);

        DetailedResult detailedResult = MBVReportMapper.map(validationReport);

        assertEquals("PASSED", detailedResult.getValidationResultsOverview().getValidationTestResult(),
                "Validation Test Result should be correctly mapped.");
    }

    /**
     * Test the mapping of Validation Test result when failed.
     */
    @Test
    public void mapValidationTestResultFailed() {
        ValidationOverview validationOverview = new ValidationOverview("disclaimer",
                "validationServiceName", "validationServiceVersion", "validatorID");
        validationOverview.setValidationOverallResult(ValidationTestResult.FAILED);
        ValidationReport validationReport = new ValidationReport(UUID, validationOverview);

        DetailedResult detailedResult = MBVReportMapper.map(validationReport);

        assertEquals("FAILED", detailedResult.getValidationResultsOverview().getValidationTestResult(),
                "Validation Test Result should be correctly mapped.");
    }

    /**
     * Test the mapping of Validation Test result when undefined.
     */
    @Test
    public void mapValidationTestResultUndefined() {
        ValidationOverview validationOverview = new ValidationOverview("disclaimer",
                "validationServiceName", "validationServiceVersion", "validatorID");
        validationOverview.setValidationOverallResult(ValidationTestResult.UNDEFINED);
        ValidationReport validationReport = new ValidationReport(UUID, validationOverview);

        DetailedResult detailedResult = MBVReportMapper.map(validationReport);

        assertEquals("UNDEFINED", detailedResult.getValidationResultsOverview().getValidationTestResult(),
                "Validation Test Result should be correctly mapped.");
    }

    /**
     * Test the mapping of an error Notification.
     */
    @Test
    public void mapConstraintsError() {
        ValidationOverview validationOverview = new ValidationOverview("disclaimer",
                "validationServiceName", "validationServiceVersion", "validatorID");
        validationOverview.setValidationOverallResult(ValidationTestResult.UNDEFINED);
        ValidationReport validationReport = new ValidationReport(UUID, validationOverview);
        ValidationSubReport validationSubReport = new ValidationSubReport("Test", new ArrayList<>());
        ConstraintValidation constraintValidation = new ConstraintValidation("constraintID", "constraintType", "constraintDescription",
                "locationInValidatedObject", "valueInValidatedObject", new ArrayList<>(),
                ConstraintPriority.MANDATORY, ValidationTestResult.FAILED);
        validationSubReport.addConstraintValidation(constraintValidation);
        validationReport.addValidationSubReport(validationSubReport);

        DetailedResult detailedResult = MBVReportMapper.map(validationReport);

        Notification notification = (Notification) detailedResult.getMDAValidation().getWarningOrErrorOrNote().get(0);
        assertTrue(notification instanceof Error, "The notification shall be an Error !");
        assertEquals("constraintDescription", notification.getDescription(), "Description shall be correctly mapped !");
        assertEquals("locationInValidatedObject", notification.getLocation(), "Location shall be correctly mapped !");
        assertEquals("constraintType", notification.getTest(), "Test shall be correctly mapped !");
    }

    /**
     * Test the mapping of a warning Notification.
     */
    @Test
    public void mapConstraintsWarning() {
        ValidationOverview validationOverview = new ValidationOverview("disclaimer",
                "validationServiceName", "validationServiceVersion", "validatorID");
        validationOverview.setValidationOverallResult(ValidationTestResult.UNDEFINED);
        ValidationReport validationReport = new ValidationReport(UUID, validationOverview);
        ValidationSubReport validationSubReport = new ValidationSubReport("Test", new ArrayList<>());
        ConstraintValidation constraintValidation = new ConstraintValidation("constraintID", "constraintType", "constraintDescription",
                "locationInValidatedObject", "valueInValidatedObject", new ArrayList<>(),
                ConstraintPriority.RECOMMENDED, ValidationTestResult.FAILED);
        validationSubReport.addConstraintValidation(constraintValidation);
        validationReport.addValidationSubReport(validationSubReport);

        DetailedResult detailedResult = MBVReportMapper.map(validationReport);

        Notification notification = (Notification) detailedResult.getMDAValidation().getWarningOrErrorOrNote().get(0);
        assertTrue(notification instanceof Warning, "The notification shall be a Warning !");
    }

    /**
     * Test the mapping of a Info Notification.
     */
    @Test
    public void mapConstraintsInfo() {
        ValidationOverview validationOverview = new ValidationOverview("disclaimer",
                "validationServiceName", "validationServiceVersion", "validatorID");
        validationOverview.setValidationOverallResult(ValidationTestResult.UNDEFINED);
        ValidationReport validationReport = new ValidationReport(UUID, validationOverview);
        ValidationSubReport validationSubReport = new ValidationSubReport("Test", new ArrayList<>());
        ConstraintValidation constraintValidation = new ConstraintValidation("constraintID", "constraintType", "constraintDescription",
                "locationInValidatedObject", "valueInValidatedObject", new ArrayList<>(),
                ConstraintPriority.PERMITTED, ValidationTestResult.FAILED);
        validationSubReport.addConstraintValidation(constraintValidation);
        validationReport.addValidationSubReport(validationSubReport);

        DetailedResult detailedResult = MBVReportMapper.map(validationReport);

        Notification notification = (Notification) detailedResult.getMDAValidation().getWarningOrErrorOrNote().get(0);
        assertTrue(notification instanceof Info, "The notification shall be a Warning !");
    }

    /**
     * Test the mapping of a Note Notification.
     */
    @Test
    public void mapConstraintsNote() {
        ValidationOverview validationOverview = new ValidationOverview("disclaimer",
                "validationServiceName", "validationServiceVersion", "validatorID");
        validationOverview.setValidationOverallResult(ValidationTestResult.UNDEFINED);
        ValidationReport validationReport = new ValidationReport(UUID, validationOverview);
        ValidationSubReport validationSubReport = new ValidationSubReport("Test", new ArrayList<>());
        ConstraintValidation constraintValidation = new ConstraintValidation("constraintID", "constraintType", "constraintDescription",
                "locationInValidatedObject", "valueInValidatedObject", new ArrayList<>(),
                ConstraintPriority.PERMITTED, ValidationTestResult.PASSED);
        validationSubReport.addConstraintValidation(constraintValidation);
        validationReport.addValidationSubReport(validationSubReport);

        DetailedResult detailedResult = MBVReportMapper.map(validationReport);

        Notification notification = (Notification) detailedResult.getMDAValidation().getWarningOrErrorOrNote().get(0);
        assertTrue(notification instanceof Note, "The notification shall be a Warning !");
    }

    /**
     * Test the mapping of sub-report result.
     */
    @Test
    public void mapSubReportResult() {
        ValidationOverview validationOverview = new ValidationOverview("disclaimer",
                "validationServiceName", "validationServiceVersion", "validatorID");
        validationOverview.setValidationOverallResult(ValidationTestResult.UNDEFINED);
        ValidationReport validationReport = new ValidationReport(UUID, validationOverview);
        ValidationSubReport validationSubReport = new ValidationSubReport("Test", new ArrayList<>());
        ConstraintValidation constraintValidation = new ConstraintValidation("constraintID", "constraintType",
                "constraintDescription", "locationInValidatedObject", "valueInValidatedObject",
                new ArrayList<>(), ConstraintPriority.MANDATORY, ValidationTestResult.FAILED);
        validationSubReport.addConstraintValidation(constraintValidation);
        validationReport.addValidationSubReport(validationSubReport);

        DetailedResult detailedResult = MBVReportMapper.map(validationReport);

        assertEquals("FAILED", detailedResult.getMDAValidation().getResult(), "The MDAValidation result is not correctly mapped !");
    }

    /**
     * Test the mapping of sub-report counter.
     */
    @Test
    public void mapSubReportCounter() {
        ValidationOverview validationOverview = new ValidationOverview("disclaimer",
                "validationServiceName", "validationServiceVersion", "validatorID");
        validationOverview.setValidationOverallResult(ValidationTestResult.UNDEFINED);
        ValidationReport validationReport = new ValidationReport(UUID, validationOverview);
        ValidationSubReport validationSubReport = new ValidationSubReport("Test", new ArrayList<>());
        ConstraintValidation constraintValidation = new ConstraintValidation("constraintID", "constraintType",
                "constraintDescription", "locationInValidatedObject", "valueInValidatedObject",
                new ArrayList<>(), ConstraintPriority.MANDATORY, ValidationTestResult.FAILED);
        validationSubReport.addConstraintValidation(constraintValidation);
        ConstraintValidation constraintValidation1 = new ConstraintValidation("constraintID", "constraintType",
                "constraintDescription", "locationInValidatedObject", "valueInValidatedObject",
                new ArrayList<>(), ConstraintPriority.RECOMMENDED, ValidationTestResult.FAILED);
        validationSubReport.addConstraintValidation(constraintValidation1);
        ConstraintValidation constraintValidation2 = new ConstraintValidation("constraintID", "constraintType",
                "constraintDescription", "locationInValidatedObject", "valueInValidatedObject",
                new ArrayList<>(), ConstraintPriority.PERMITTED, ValidationTestResult.FAILED);
        validationSubReport.addConstraintValidation(constraintValidation2);
        ConstraintValidation constraintValidation3 = new ConstraintValidation("constraintID", "constraintType",
                "constraintDescription", "locationInValidatedObject", "valueInValidatedObject",
                new ArrayList<>(), ConstraintPriority.MANDATORY, ValidationTestResult.PASSED);
        validationSubReport.addConstraintValidation(constraintValidation3);
        validationReport.addValidationSubReport(validationSubReport);

        DetailedResult detailedResult = MBVReportMapper.map(validationReport);

        assertEquals(BigInteger.valueOf(4), detailedResult.getMDAValidation().getValidationCounters().getNrOfChecks(), "The Nbr of checks is not " +
                "correctly mapped !");
        assertEquals(BigInteger.valueOf(1), detailedResult.getMDAValidation().getValidationCounters().getNrOfValidationErrors(), "The Nbr of Errors" +
                " is not correctly mapped !");
        assertEquals(BigInteger.valueOf(1), detailedResult.getMDAValidation().getValidationCounters().getNrOfValidationWarnings(), "The Nbr of " +
                "Warnings is not correctly mapped !");
        assertEquals(BigInteger.valueOf(1), detailedResult.getMDAValidation().getValidationCounters().getNrOfValidationInfos(), "The Nbr of Infos " +
                "is not correctly mapped !");
    }

    /**
     * Test the mapping of constraints assertions
     */
    @Test
    public void mapConstraintAssertion() {
        String assertionId = "assertionId";
        ValidationOverview validationOverview = new ValidationOverview("disclaimer",
                "validationServiceName", "validationServiceVersion", "validatorID");
        validationOverview.setValidationOverallResult(ValidationTestResult.UNDEFINED);
        ValidationReport validationReport = new ValidationReport(UUID, validationOverview);
        ValidationSubReport validationSubReport = new ValidationSubReport("Test", new ArrayList<>());
        List<String> assertionIds = new ArrayList<>();
        assertionIds.add(assertionId);
        ConstraintValidation constraintValidation = new ConstraintValidation("constraintID", "constraintType",
                "constraintDescription", "locationInValidatedObject", "valueInValidatedObject",
                assertionIds, ConstraintPriority.MANDATORY, ValidationTestResult.FAILED);
        validationSubReport.addConstraintValidation(constraintValidation);
        validationReport.addValidationSubReport(validationSubReport);

        DetailedResult detailedResult = MBVReportMapper.map(validationReport);

        Notification notification = (Notification) detailedResult.getMDAValidation().getWarningOrErrorOrNote().get(0);
        assertEquals(1, notification.getAssertions().size(), "The Assertions are not correctly mapped !");
        assertEquals(assertionId, notification.getAssertions().get(0).getAssertionId(), "The Assertions are not correctly mapped !");
    }

    /**
     * Test the marshalling of a {@link DetailedResult} to an XML String
     */
    @Test
    public void getDetailedResultAsString() {
        DetailedResult detailedResult = new DetailedResult();

        assertNotNull(MBVReportMapper.getDetailedResultAsString(detailedResult), "Marshaled DetailedResult shall not be null !");
    }
}