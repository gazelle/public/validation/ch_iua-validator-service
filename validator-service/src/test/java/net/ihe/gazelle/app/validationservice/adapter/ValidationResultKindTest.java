package net.ihe.gazelle.app.validationservice.adapter;


import net.ihe.gazelle.app.validationservice.adapter.templates.ValidationResultKind;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class ValidationResultKindTest {

    @Test
    public void testGetKind() {
        assertTrue(ValidationResultKind.ERROR.getKind().equals("Error"));
    }

}
