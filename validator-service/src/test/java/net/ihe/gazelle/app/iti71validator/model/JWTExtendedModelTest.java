package net.ihe.gazelle.app.iti71validator.model;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.LongNode;
import com.fasterxml.jackson.databind.node.ShortNode;
import com.fasterxml.jackson.databind.node.TextNode;
import net.ihe.gazelle.app.iti71validator.model.*;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

class JWTExtendedModelTest {

    private static final String PRINCIPAL = "Martina Musterarzt";
    private static final String PRINCIPAL_ID = "2000000090092";
    private final ChDelegation chDelegation = new ChDelegation(PRINCIPAL, PRINCIPAL_ID);

    private static final String USER_ID = "2000000090108";
    private static final String USER_ID_QUALIFIER = "urn:gs1:gln";
    private final ChEPRExtension chEPRExtension = new ChEPRExtension(USER_ID, USER_ID_QUALIFIER);

    private static final String NAME1 = "Name of group with id urn:oid:2.2.2.1";
    private static final String ID1 = "urn:oid:2.2.2.1";
    private static final String NAME2 = "Name of group with id urn:oid:2.2.2.2";
    private static final String ID2 = "urn:oid:2.2.2.2";
    private final GroupModel groupModel1 = new GroupModel(NAME1, ID1);
    private final GroupModel groupModel2 = new GroupModel(NAME2, ID2);
    private final ChGroupExtension chGroupExtension = new ChGroupExtension(new ArrayList<>(Arrays.asList(groupModel1, groupModel2)));

    private static final String SYSTEM = "urn:uuid:2.16.756.5.30.1.127.3.10.5";
    private static final String CODE = "NORM";
    private final SubjectRoleModel subjectRole = new SubjectRoleModel(SYSTEM, CODE);
    private final static String SUBJECT_NAME = "Martina Musterarzt";
    private final static String PERSON_ID = "761337610411353650^^^&amp;2.16.756.5.30.1.127.3.10.3&amp;ISO";
    private static final String SYSTEM2 = "urn:uuid:2.16.756.5.30.1.127.3.10.6";
    private static final String CODE2 = "EMER";
    private final PurposeOfUseModel purposeOfUse = new PurposeOfUseModel(SYSTEM2, CODE2);
    private final IheIUAExtension iheIUAExtension = new IheIUAExtension(SUBJECT_NAME, PERSON_ID, subjectRole, purposeOfUse);

    private final JWTExtension jwtExtension = new JWTExtension(iheIUAExtension, chEPRExtension, chGroupExtension, chDelegation);

    private static final JsonNode ISS = TextNode.valueOf("http://issuerAdress.ch");
    private static final JsonNode SUB = TextNode.valueOf("UserId-bfe8a208-b9d0-4012-b2f5-168b949fc3cb");
    private static final JsonNode AUD = TextNode.valueOf("http://mhdResourceServerURL.ch");
    private static final JsonNode EXP = LongNode.valueOf(Long.parseLong("1587294580000"));
    private static final JsonNode NBF = LongNode.valueOf(Long.parseLong("1587294580000"));
    private static final JsonNode IAT = LongNode.valueOf(Long.parseLong("1587294580000"));
    private static final JsonNode CLIENTID = TextNode.valueOf("client id test");
    private static final JsonNode SCOPE = TextNode.valueOf("scope test");
    private static final JsonNode JTI = TextNode.valueOf("c5436729-3f26-4dbf-abd3-2790dc7771a");
    private final JWTTokenExtended jwtTokenExtended = JWTTokenExtended.builder().withIssuer(ISS).withSubject(SUB).withAudience(AUD).withExpiration(EXP).withNotBefore(NBF).withIssuedAt(IAT).withClientId(CLIENTID).withScope(SCOPE).withJwtId(JTI).withExtension(jwtExtension).build();


    @Test
    void getterAndSetterChDelegationTest() {
        Assertions.assertEquals(PRINCIPAL, chDelegation.getPrincipal());
        Assertions.assertEquals(PRINCIPAL_ID, chDelegation.getPrincipalId());
        String newPrincipal = "Martina test";
        String newPrincipalId = "3003";
        chDelegation.setPrincipal(newPrincipal);
        chDelegation.setPrincipalId(newPrincipalId);
        Assertions.assertEquals(chDelegation.getPrincipal(), newPrincipal);
        Assertions.assertEquals(chDelegation.getPrincipalId(), newPrincipalId);
    }

    @Test
    void getNonNullParamChDelegationTest() {
        Assertions.assertEquals(2, chDelegation.getNonNullParamAsJWTParameterValue().size());
    }

    @Test
    void getterAndSetterChEPRExtensionTest() {
        Assertions.assertEquals(USER_ID, chEPRExtension.getUserId());
        Assertions.assertEquals(USER_ID_QUALIFIER, chEPRExtension.getUserIdQualifier());
        String newUserId= "4527524524";
        String newUserIdQualifier= "urn:gs1:glt";
        chEPRExtension.setUserId(newUserId);
        chEPRExtension.setUserIdQualifier(newUserIdQualifier);
        Assertions.assertEquals(chEPRExtension.getUserId(), newUserId);
        Assertions.assertEquals(chEPRExtension.getUserIdQualifier(), newUserIdQualifier);
    }

    @Test
    void getNonNullParamChEPRExtensionTest() {
        Assertions.assertEquals(2, chEPRExtension.getNonNullParamAsJWTParameterValue().size());
    }

    @Test
    void getterAndSetterGroupModelTest() {
        Assertions.assertEquals(NAME1, groupModel1.getName());
        Assertions.assertEquals(ID1, groupModel1.getId());
        String newName = "Name of group with id urn:oid:2.2.2.3";
        String newId = "urn:oid:2.2.2.3";
        groupModel1.setName(newName);
        groupModel1.setId(newId);
        Assertions.assertEquals(groupModel1.getName(), newName);
        Assertions.assertEquals(groupModel1.getId(), newId);
    }

    @Test
    void getNonNullParamGroupModelTest() {
        Assertions.assertEquals(2, chEPRExtension.getNonNullParamAsJWTParameterValue().size());
    }

    @Test
    void getterAndSetterChGroupExtensionTest() {
        Assertions.assertEquals(2, chGroupExtension.getGroups().size());
        Assertions.assertEquals(groupModel1, chGroupExtension.getGroups().get(0));
        Assertions.assertEquals(groupModel2, chGroupExtension.getGroups().get(1));
        String newName = "Name of group with id urn:oid:2.2.2.2";
        String newId = "urn:oid:2.2.2.3";
        GroupModel groupModel3 = new GroupModel(newName, newId);
        List<GroupModel> groups = new ArrayList<>(Arrays.asList(groupModel3, groupModel1, groupModel2));
        chGroupExtension.setGroups(groups);
        Assertions.assertEquals(3, chGroupExtension.getGroups().size());
        Assertions.assertEquals(groupModel3, chGroupExtension.getGroups().get(0));
        Assertions.assertEquals(groupModel1, chGroupExtension.getGroups().get(1));
        Assertions.assertEquals(groupModel2, chGroupExtension.getGroups().get(2));
    }

    @Test
    void getNonNullParamChGroupExtensionTest() {
        Assertions.assertEquals(2, chGroupExtension.getNonNullParamAsJWTParameterValue().size());
    }

    @Test
    void getterAndSetterSubjectRoleModelTest() {
        Assertions.assertEquals(SYSTEM, subjectRole.getSystem());
        Assertions.assertEquals(CODE, subjectRole.getCode());
        String newSystem = "urn:uuid:2.16.756.5.30.1.127.3.10.6";
        String newCode = "EMER";
        subjectRole.setSystem(newSystem);
        subjectRole.setCode(newCode);
        Assertions.assertEquals(subjectRole.getSystem(), newSystem);
        Assertions.assertEquals(subjectRole.getCode(), newCode);
    }

    @Test
    void getNonNullParamSubjectRoleModelTest() {
        Assertions.assertEquals(2, subjectRole.getNonNullParamAsJWTParameterValue().size());
    }

    @Test
    void getterAndSetterPurposeOfUseModelTest() {
        Assertions.assertEquals(SYSTEM2, purposeOfUse.getSystem());
        Assertions.assertEquals(CODE2, purposeOfUse.getCode());
        String newSystem = "urn:uuid:2.16.756.5.30.1.127.3.10.6";
        String newCode = "EMER";
        purposeOfUse.setSystem(newSystem);
        purposeOfUse.setCode(newCode);
        Assertions.assertEquals(purposeOfUse.getSystem(), newSystem);
        Assertions.assertEquals(purposeOfUse.getCode(), newCode);
    }

    @Test
    void getNonNullParamPurposeOfUseModelTest() {
        Assertions.assertEquals(2, purposeOfUse.getNonNullParamAsJWTParameterValue().size());
    }

    @Test
    void getterAndSetterIheIUAExtensionTest() {
        Assertions.assertEquals(SUBJECT_NAME, iheIUAExtension.getSubjectName());
        Assertions.assertEquals(PERSON_ID, iheIUAExtension.getPersonId());
        Assertions.assertEquals(subjectRole, iheIUAExtension.getSubjectRole());
        Assertions.assertEquals(purposeOfUse, iheIUAExtension.getPurposeOfUse());
        String newSubjectName = "Martina test";
        String newPersonId= "761337610411353650^^^&amp;2.16.756.5.30.1.127.3.10.4&amp;ISO";
        SubjectRoleModel newSubjectRole = new SubjectRoleModel("urn:uuid:2.16.756.5.30.1.127.3.10.7", "NORM");
        PurposeOfUseModel newPurposeOfUse = new PurposeOfUseModel("urn:uuid:2.16.756.5.30.1.127.3.10.8", "EMER");
        iheIUAExtension.setSubjectName(newSubjectName);
        iheIUAExtension.setPersonId(newPersonId);
        iheIUAExtension.setSubjectRole(newSubjectRole);
        iheIUAExtension.setPurposeOfUse(newPurposeOfUse);
        Assertions.assertEquals(iheIUAExtension.getSubjectName(), newSubjectName);
        Assertions.assertEquals(iheIUAExtension.getPersonId(), newPersonId);
        Assertions.assertEquals(iheIUAExtension.getSubjectRole(), newSubjectRole);
        Assertions.assertEquals(iheIUAExtension.getPurposeOfUse(), newPurposeOfUse);
    }

    @Test
    void getNonNullParamIheIUAExtensionTest() {
        Assertions.assertEquals(6, iheIUAExtension.getNonNullParamAsJWTParameterValue().size());
    }

    @Test
    void getterAndSetterJWTExtensionTest() {
        Assertions.assertEquals(iheIUAExtension, jwtExtension.getIheIUAExtension());
        Assertions.assertEquals(chEPRExtension, jwtExtension.getChEPRExtension());
        Assertions.assertEquals(chGroupExtension, jwtExtension.getChGroupExtension());
        Assertions.assertEquals(chDelegation, jwtExtension.getChDelegation());
        jwtExtension.setIheIUAExtension(iheIUAExtension);
        jwtExtension.setChEPRExtension(chEPRExtension);
        jwtExtension.setChGroupExtension(chGroupExtension);
        jwtExtension.setChDelegation(chDelegation);
        Assertions.assertEquals(jwtExtension.getIheIUAExtension(), iheIUAExtension);
        Assertions.assertEquals(jwtExtension.getChEPRExtension(), chEPRExtension);
        Assertions.assertEquals(jwtExtension.getChGroupExtension(), chGroupExtension);
        Assertions.assertEquals(jwtExtension.getChDelegation(), chDelegation);
    }

    @Test
    void getNonNullParamJWTExtensionTest() {
        Assertions.assertEquals(12, jwtExtension.getNonNullParamAsJWTParameterValue().size());
    }

    @Test
    void getterAndSetterJWTTokenExtendedTest() {
        Assertions.assertEquals(ISS.textValue(), jwtTokenExtended.getIssuer());
        Assertions.assertEquals(SUB.textValue(), jwtTokenExtended.getSubject());
        Assertions.assertEquals(AUD.textValue(), jwtTokenExtended.getAudience());
        Assertions.assertEquals(String.valueOf(EXP.longValue()), jwtTokenExtended.getExpiration());
        Assertions.assertEquals(String.valueOf(NBF.longValue()), jwtTokenExtended.getNotBefore());
        Assertions.assertEquals(String.valueOf(IAT.longValue()), jwtTokenExtended.getIssuedAt());
        Assertions.assertEquals(CLIENTID.textValue(), jwtTokenExtended.getClientId());
        Assertions.assertEquals(SCOPE.textValue(), jwtTokenExtended.getScope());
        Assertions.assertEquals(JTI.textValue(), jwtTokenExtended.getJwtId());
    }

    @Test
    void getNonNullParamJWTTokenExtendedTest() {
        Assertions.assertEquals(21, jwtTokenExtended.getNonNullParamAsJWTParameterValue().size());
    }
}
