package net.ihe.gazelle.app.iti71validator.application;

import net.ihe.gazelle.app.iti71validator.beans.CHIUAObject;
import net.ihe.gazelle.app.iti71validator.beans.IUAGetURLValidatorDescription;
import net.ihe.gazelle.app.iti71validator.business.IUARequestParameter;
import net.ihe.gazelle.app.iti71validator.business.ParameterType;

import java.util.ArrayList;
import java.util.List;

public class IUAURLValidatorTestData {
    private static final String HTTP_GET = "get";
    private static final String HTTP_POST = "post";

    private IUAURLValidatorTestData() {
    }

    static final String URL1_FULL = "http://baseurl.com/authorize?response_type=code&client_id=app-client-id&http%3A%2F%2Flocalhost%3A9000%2Fcallback&launch=xyz123&scope=launch+user%2F%2A.%2A+openid+fhirUser&state=98wrghuwuogerg97&aud=https%3A%2F%2Fehr%2Ffhir&code_challenge=ZmVjMmIwMWYyYTNjZWJiNTgyNTgxYzlmOGYyMWM0MWI3YmZhMjQ4YjU5MDc3Mzk4MDBmYTk0OThlNzZiNjAwMw&code_challenge_method=S256";
    static final String URL1_BASE = "http://baseurl.com/authorize";
    static final String URL1_PARAMETERS = "response_type=code&client_id=app-client-id&http%3A%2F%2Flocalhost%3A9000%2Fcallback&launch=xyz123&scope=launch+user%2F%2A.%2A+openid+fhirUser&state=98wrghuwuogerg97&aud=https%3A%2F%2Fehr%2Ffhir&code_challenge=ZmVjMmIwMWYyYTNjZWJiNTgyNTgxYzlmOGYyMWM0MWI3YmZhMjQ4YjU5MDc3Mzk4MDBmYTk0OThlNzZiNjAwMw&code_challenge_method=S256";
    static final String URL1_FIRST_PARAMETER = "response_type=code";
    static final String URL1_SECOND_PARAMETER = "client_id=app-client-id";
    static final String URL1_THIRD_PARAMETER = "http%3A%2F%2Flocalhost%3A9000%2Fcallback";
    static final String URL1_FOURTH_PARAMETER = "launch=xyz123";
    static final String URL1_FIFTH_PARAMETER = "scope=launch+user%2F%2A.%2A+openid+fhirUser";
    static final String URL1_SIXTH_PARAMETER = "state=98wrghuwuogerg97";
    static final String URL1_SEVENTH_PARAMETER = "aud=https%3A%2F%2Fehr%2Ffhir";
    static final String URL1_EIGHTH_PARAMETER = "code_challenge=ZmVjMmIwMWYyYTNjZWJiNTgyNTgxYzlmOGYyMWM0MWI3YmZhMjQ4YjU5MDc3Mzk4MDBmYTk0OThlNzZiNjAwMw";
    static final String URL1_NINTH_PARAMETER = "code_challenge_method=S256";

    static final String URL2_FULL = "http://baseurl.com/authorize?response_type=code&client_id=app-client-id&http%3A%2F%2Flocalhost%3A9000%2Fcallback&launch=xyz123&scope=launch+user%2F*.*+openid+fhirUser+purpose_of_use%3Durn%3Aoid%3A2.16.756.5.30.1.127.3.10.5%7CNORM+subject_role%3Durn%3Aoid%3A2.16.756.5.30.1.127.3.10.6%7CHCP+person_id%3D761337610411353650%5E%5E%5E%26amp%3B2.16.756.5.30.1.127.3.10.3%26amp%3BISO&state=98wrghuwuogerg97&aud=https%3A%2F%2Fehr%2Ffhir&code_challenge=ZmVjMmIwMWYyYTNjZWJiNTgyNTgxYzlmOGYyMWM0MWI3YmZhMjQ4YjU5MDc3Mzk4MDBmYTk0OThlNzZiNjAwMw&code_challenge_method=S256";
    static final String URL2_PARAMETERS = "response_type=code&client_id=app-client-id&http%3A%2F%2Flocalhost%3A9000%2Fcallback&launch=xyz123&scope=launch+user%2F*.*+openid+fhirUser+purpose_of_use%3Durn%3Aoid%3A2.16.756.5.30.1.127.3.10.5%7CNORM+subject_role%3Durn%3Aoid%3A2.16.756.5.30.1.127.3.10.6%7CHCP+person_id%3D761337610411353650%5E%5E%5E%26amp%3B2.16.756.5.30.1.127.3.10.3%26amp%3BISO&state=98wrghuwuogerg97&aud=https%3A%2F%2Fehr%2Ffhir&code_challenge=ZmVjMmIwMWYyYTNjZWJiNTgyNTgxYzlmOGYyMWM0MWI3YmZhMjQ4YjU5MDc3Mzk4MDBmYTk0OThlNzZiNjAwMw&code_challenge_method=S256";
    static final String URL2_FIRST_PARAMETER = "response_type=code";
    static final String URL2_SECOND_PARAMETER = "client_id=app-client-id";
    static final String URL2_THIRD_PARAMETER = "http%3A%2F%2Flocalhost%3A9000%2Fcallback";
    static final String URL2_FOURTH_PARAMETER = "launch=xyz123";
    static final String URL2_FIFTH_PARAMETER = "scope=launch+user%2F*.*+openid+fhirUser+purpose_of_use%3Durn%3Aoid%3A2.16.756.5.30.1.127.3.10.5%7CNORM+subject_role%3Durn%3Aoid%3A2.16.756.5.30.1.127.3.10.6%7CHCP+person_id%3D761337610411353650%5E%5E%5E%262.16.756.5.30.1.127.3.10.3%26ISO";
    static final String URL2_SIXTH_PARAMETER = "state=98wrghuwuogerg97";
    static final String URL2_SEVENTH_PARAMETER = "aud=https%3A%2F%2Fehr%2Ffhir";
    static final String URL2_EIGHTH_PARAMETER = "code_challenge=ZmVjMmIwMWYyYTNjZWJiNTgyNTgxYzlmOGYyMWM0MWI3YmZhMjQ4YjU5MDc3Mzk4MDBmYTk0OThlNzZiNjAwMw";
    static final String URL2_NINTH_PARAMETER = "code_challenge_method=S256";

    static final String URL2_FIRST_PARAMETER_KEY = "response_type";
    static final String URL2_FIRST_PARAMETER_VALUE = "code";

    static final String PARAMETER_RESPONSETYPE = "response_type";
    static final String PARAMETER_CLIENTID = "client_id";
    static final String PARAMETER_REDIRECTURI = "redirect_uri";
    static final String PARAMETER_LAUNCH = "launch";
    static final String PARAMETER_SCOPE = "scope";
    static final String PARAMETER_STATE = "state";
    static final String PARAMETER_AUD = "aud";
    static final String PARAMETER_CODECHALLENGE = "code_challenge";
    static final String PARAMETER_CODECHALLENGEMETHOD = "code_challenge_method";
    static final String PARAMETER_SCOPE_LAUNCH = "launch";
    static final String PARAMETER_SCOPE_PURPOSEOFUSE = "purpose_of_use";
    static final String PARAMETER_SCOPE_SUBJECTROLE = "subject_role";
    static final String PARAMETER_SCOPE_PERSONID = "person_id";
    static final String PARAMETER_SCOPE_PRINCIPAL = "principal";
    static final String PARAMETER_SCOPE_PRINCIPALID = "principal_id";
    static final String PARAMETER_SCOPE_GROUP = "group";
    static final String PARAMETER_SCOPE_GROUPID = "group_id";
    static final String PARAMETER_SCOPE_ACCESSTOKENFORMAT = "access_token_format";
    static final String LOCALHOST = "localhost";
    static final String URL_NO_PROTOCOL = "baseurl.com/authorize";
    static final String HTTP_URL = "http://" + URL_NO_PROTOCOL;
    static final String HTTPS_URL = "https://" + URL_NO_PROTOCOL;
    static final String BASE_URL_GET = "http://baseurl.com/authorize";


    static final IUARequestParameter PARAMETER_RESPONSETYPE_DESCRIPTION = new IUARequestParameter(URL2_FIRST_PARAMETER_VALUE, ParameterType.STRING, true, "code");
    static final IUARequestParameter PARAMETER_CLIENTID_DESCRIPTION = new IUARequestParameter(PARAMETER_CLIENTID, ParameterType.ID, true, null);
    static final IUARequestParameter PARAMETER_LAUNCH_DESCRIPTION = new IUARequestParameter(PARAMETER_LAUNCH, ParameterType.URI, true, null);
    static final IUARequestParameter PARAMETER_SCOPE_DESCRIPTION = new IUARequestParameter(PARAMETER_SCOPE, ParameterType.STRING, true, null);
    static final IUARequestParameter PARAMETER_STATE_DESCRIPTION = new IUARequestParameter(PARAMETER_STATE, ParameterType.STRING, true, null);
    static final IUARequestParameter PARAMETER_AUD_DESCRIPTION = new IUARequestParameter(PARAMETER_AUD, ParameterType.STRING, true, null);
    static final IUARequestParameter PARAMETER_CODECHALLENGE_DESCRIPTION = new IUARequestParameter(PARAMETER_CODECHALLENGE, ParameterType.STRING, true, null);
    static final IUARequestParameter PARAMETER_CODECHALLENGEMETHOD_DESCRIPTION = new IUARequestParameter(PARAMETER_CODECHALLENGEMETHOD, ParameterType.STRING, true, "S256");
    static final IUARequestParameter PARAMETER_SCOPE_LAUNCH_DESCRIPTION = new IUARequestParameter(PARAMETER_SCOPE_LAUNCH, ParameterType.URI, true, null);
    static final IUARequestParameter PARAMETER_SCOPE_PERSONID_DESCRIPTION = new IUARequestParameter(PARAMETER_SCOPE_PERSONID, ParameterType.STRING, true, "S256");
    static final IUARequestParameter PARAMETER_SCOPE_PURPOSEOFUSE_DESCRIPTION= new IUARequestParameter(PARAMETER_SCOPE_PURPOSEOFUSE, ParameterType.STRING, true, "S256");
    static final IUARequestParameter PARAMETER_SCOPE_SUBJECTROLE_DESCRIPTION = new IUARequestParameter(PARAMETER_SCOPE_SUBJECTROLE, ParameterType.STRING, true, "S256");
    static final IUARequestParameter PARAMETER_SCOPE_PRINCIPAL_DESCRIPTION = new IUARequestParameter(PARAMETER_SCOPE_PRINCIPAL, ParameterType.STRING, true, "S256");
    static final IUARequestParameter PARAMETER_SCOPE_PRINCIPALID_DESCRIPTION = new IUARequestParameter(PARAMETER_SCOPE_PRINCIPALID, ParameterType.STRING, true, "S256");
    static final IUARequestParameter PARAMETER_SCOPE_GROUP_DESCRIPTION = new IUARequestParameter(PARAMETER_SCOPE_GROUP, ParameterType.STRING, true, "S256");
    static final IUARequestParameter PARAMETER_SCOPE_GROUPID_DESCRIPTION = new IUARequestParameter(PARAMETER_SCOPE_GROUPID, ParameterType.STRING, true, "S256");
    static final IUARequestParameter PARAMETER_SCOPE_ACCESSTOKENFORMAT_DESCRIPTION = new IUARequestParameter(PARAMETER_SCOPE_ACCESSTOKENFORMAT, ParameterType.STRING, true, "S256");
    static final String KO_PARAMETER = "test";
    static final IUARequestParameter KO_PARAMETER_DESCRIPTION = new IUARequestParameter(KO_PARAMETER, ParameterType.STRING, true, "ko");

    public static IUAGetURLValidatorDescription getValidatorDescriptionForGetTest() {
        return new IUAGetURLValidatorDescription(true, URL2_FULL);
    }

    public static List<String> getUsedParameterCompleteList(){
        List<String> parameters = new ArrayList<>();
        parameters.add(PARAMETER_RESPONSETYPE);
        parameters.add(PARAMETER_CLIENTID);
        parameters.add(PARAMETER_LAUNCH);
        parameters.add(PARAMETER_SCOPE);
        parameters.add(PARAMETER_STATE);
        parameters.add(PARAMETER_AUD);
        parameters.add(PARAMETER_REDIRECTURI);
        parameters.add(PARAMETER_CODECHALLENGE);
        parameters.add(PARAMETER_CODECHALLENGEMETHOD);
        parameters.add(PARAMETER_SCOPE_LAUNCH);
        parameters.add(PARAMETER_SCOPE_PERSONID);
        parameters.add(PARAMETER_SCOPE_PURPOSEOFUSE);
        parameters.add(PARAMETER_SCOPE_SUBJECTROLE);
        parameters.add(PARAMETER_SCOPE_PRINCIPAL);
        parameters.add(PARAMETER_SCOPE_PRINCIPALID);
        parameters.add(PARAMETER_SCOPE_GROUP);
        parameters.add(PARAMETER_SCOPE_GROUPID);
        parameters.add(PARAMETER_SCOPE_ACCESSTOKENFORMAT);
        return parameters;
    }
}