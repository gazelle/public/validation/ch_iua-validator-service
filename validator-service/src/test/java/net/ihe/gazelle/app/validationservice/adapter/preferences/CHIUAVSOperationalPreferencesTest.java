package net.ihe.gazelle.app.validationservice.adapter.preferences;

import org.junit.jupiter.api.Test;

import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

class CHIUAVSOperationalPreferencesTest {

    @Test
    void wantedMandatoryPreferences() {
        CHIUAVSOperationalPreferences preferences = new CHIUAVSOperationalPreferences();

        Map mandatoryPreferences = preferences.wantedMandatoryPreferences();

        assertNotNull(mandatoryPreferences, "Mandatory Preferences map shall not be null !");
        assertEquals(0, mandatoryPreferences.keySet().size(), "Mandatory Preferences map shall be empty !");
    }
}