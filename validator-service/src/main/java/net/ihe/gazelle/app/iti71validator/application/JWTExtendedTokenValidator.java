package net.ihe.gazelle.app.iti71validator.application;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import net.ihe.gazelle.app.iti71validator.beans.JWTValidatorDescription;
import net.ihe.gazelle.app.iti71validator.business.JWTParameterKey;
import net.ihe.gazelle.app.iti71validator.business.JWTParameterValue;
import net.ihe.gazelle.app.iti71validator.model.*;
import net.ihe.gazelle.app.validationservice.adapter.reportmodel.*;
import net.ihe.gazelle.app.validationservice.adapter.reportmodel.Error;

import java.io.IOException;
import java.math.BigInteger;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.regex.Pattern;

public class JWTExtendedTokenValidator {
    private final MDAValidation validationResult;
    private final DocumentValidXSD documentValidXSD;
    private final DocumentWellFormed documentWellFormed;
    private int errorCount = 0;
    private int noteCount = 0;

    public JWTExtendedTokenValidator() {
        validationResult = new MDAValidation();
        documentValidXSD = new DocumentValidXSD();
        documentWellFormed = new DocumentWellFormed();
    }

    public DocumentValidXSD getDocumentValidXSD() {
        return documentValidXSD;
    }

    public DocumentWellFormed getDocumentWellFormed() {
        return documentWellFormed;
    }

    private void addError(Notification error) {
        validationResult.getWarningOrErrorOrNote().add(error);
        errorCount++;
    }

    private void addNote(Notification note) {
        validationResult.getWarningOrErrorOrNote().add(note);
        noteCount++;
    }

    private void addValidationCounters() {
        ValidationCounters counters = new ValidationCounters();
        counters.setNrOfValidationWarnings(BigInteger.ZERO);
        counters.setNrOfValidationInfos(BigInteger.ZERO);
        counters.setNrOfValidationNotes(BigInteger.valueOf(noteCount));
        counters.setNrOfValidationErrors(BigInteger.valueOf(errorCount));
        counters.setNrOfChecks(BigInteger.valueOf((long) noteCount + errorCount));
        validationResult.setValidationCounters(counters);
    }

    public MDAValidation validateRequest(JWTValidatorDescription jwtValidatorDescription) {
        JWTTokenExtended jwtTokenExtended = isJsonValidFormat(jwtValidatorDescription.getJwt());
        if (jwtTokenExtended != null) {
            checkAllRequiredParametersArePresent(jwtTokenExtended);
            validateJWT(jwtTokenExtended);
            addValidationCounters();
            String validationStatus = errorCount == 0 ? ReportCreator.PASSED : ReportCreator.FAILED;
            validationResult.setResult(validationStatus);
        } else {
            validationResult.setResult(ReportCreator.FAILED);
            errorCount++;
        }
        return validationResult;
    }

    public void validateJWT(JWTTokenExtended jwtTokenExtended) {
        for (JWTParameterValue jwtParameterValue : jwtTokenExtended.getNonNullParamAsJWTParameterValue().keySet()) {
            Object value = jwtTokenExtended.getNonNullParamAsJWTParameterValue().get(jwtParameterValue);
            validateParameterValueFormat(jwtParameterValue, value);
        }
    }

    protected void validateParameterValueFormat(JWTParameterValue testedParameter, Object value) {
        String regex = testedParameter.getRegex();
        if (regex == null || regex.isEmpty()) {
            regex = testedParameter.getType().getDefaultRegex();
        }
        final Pattern pattern = Pattern.compile(regex);
        if (value instanceof String) {
            if (pattern.matcher((String) value).matches()) {
                Notification note = new Note();
                note.setDescription("Value for parameter " + testedParameter.name().toLowerCase() + " is correctly formatted");
                note.setLocation((String) value);
                addNote(note);
            } else {
                Notification error = new Error();
                error.setDescription("Value for parameter " + testedParameter.name().toLowerCase() + " shall match regex " + regex);
                error.setLocation((String) value);
                addError(error);
            }
        } else if (value instanceof ArrayList) {
            int i = 0;
            for (String val : (ArrayList<String>) value) {
                if (pattern.matcher(val).matches()) {
                    Notification note = new Note();
                    note.setDescription("Value for parameter " + testedParameter.name().toLowerCase() + "[" + i + "] is correctly formatted");
                    note.setLocation(val);
                    addNote(note);
                } else {
                    Notification error = new Error();
                    error.setDescription("Value for parameter " + testedParameter.name().toLowerCase() + "[" + i + "] shall match regex " + regex);
                    error.setLocation(val);
                    addError(error);
                }
                i++;
            }
        }
    }

    public void checkAllRequiredParametersArePresent(JWTTokenExtended jwtTokenExtended) {
        List<JWTParameterValue> requiredParameters = getRequiredParametersForRequest();
        for (JWTParameterValue requiredParam : requiredParameters) {
            if (jwtTokenExtended.getNonNullParamAsJWTParameterValue().containsKey(requiredParam)) {
                Notification note = new Note();
                note.setDescription(requiredParam.getKey() + " is required and present");
                addNote(note);
            } else {
                Notification error = new Error();
                error.setDescription("A parameter named " + requiredParam.getKey() + " is required but missing in this URL");
                addError(error);
            }
        }
    }

    public List<JWTParameterValue> getRequiredParametersForRequest() {
        List<JWTParameterValue> allParameters = JWTParameterValue.getAllParameters();
        List<JWTParameterValue> requiredParameters = new ArrayList<>();
        for (JWTParameterValue parameter : allParameters) {
            if (parameter.isRequired()) {
                requiredParameters.add(parameter);
            }
        }
        return requiredParameters;
    }

    public JWTTokenExtended isJsonValidFormat(String json) {
        try {
            ObjectMapper mapper = new ObjectMapper();
            JsonNode node = mapper.readValue(json, JsonNode.class);
            this.documentWellFormed.setResult("PASSED");
            return wellFormedNessJWT(node);
        } catch (IOException e) {
            this.documentWellFormed.setResult("FAILED");
            XSDMessage xsdMessage = new XSDMessage();
            xsdMessage.setMessage(e.getMessage());
            this.documentWellFormed.getXSDMessage().add(xsdMessage);
        }
        return null;
    }

    public JWTTokenExtended wellFormedNessJWT(JsonNode node) {
        try {
            JWTTokenExtended jwtTokenExtended = jsonNodeToJWTExtended(node);
            this.documentValidXSD.setResult("PASSED");
            return jwtTokenExtended;
        } catch (ParseException e) {
            this.documentValidXSD.setResult("FAILED");
            XSDMessage xsdMessage = new XSDMessage();
            xsdMessage.setMessage(e.getMessage());
            this.documentValidXSD.getXSDMessage().add(xsdMessage);
        }
        return null;
    }

    public JWTTokenExtended jsonNodeToJWTExtended(JsonNode node) throws ParseException {
        return JWTTokenExtended.builder()
                .withIssuer(node.findValue(JWTParameterKey.ISSUER.getKey()))
                .withSubject(node.findValue(JWTParameterKey.SUBJECT.getKey()))
                .withAudience(node.findValue(JWTParameterKey.AUDIENCE.getKey()))
                .withExpiration(node.findValue(JWTParameterKey.EXPIRATION.getKey()))
                .withNotBefore(node.findValue(JWTParameterKey.NOTBEFORE.getKey()))
                .withIssuedAt(node.findValue(JWTParameterKey.ISSUEDAT.getKey()))
                .withClientId(node.findValue(JWTParameterKey.CLIENTID.getKey()))
                .withScope(node.findValue(JWTParameterKey.SCOPE.getKey()))
                .withJwtId(node.findValue(JWTParameterKey.JWTID.getKey()))
                .withExtension(jsonNodeToJWTExt(node.findValue(JWTParameterKey.EXTENSION.getKey())))
                .build();
    }

    public JWTExtension jsonNodeToJWTExt(JsonNode node) throws ParseException {
        if (node == null) {
            return null;
        }
        IheIUAExtension iheIUAExtension = null;
        ChEPRExtension chEPRExtension = null;
        ChGroupExtension chGroupExtension = null;
        ChDelegation chDelegation = null;
        if (node.findValue(JWTParameterKey.IHEIUA.getKey()) != null) {
            iheIUAExtension = jsonNodeToIheIUAExtension(node.findValue(JWTParameterKey.IHEIUA.getKey()));
        }
        if (node.findValue(JWTParameterKey.CHEPR.getKey()) != null) {
            chEPRExtension = jsonNodeToChEPRExtension(node.findValue(JWTParameterKey.CHEPR.getKey()));
        }
        if (node.findValue(JWTParameterKey.CHGROUP.getKey()) != null) {
            chGroupExtension = jsonNodeToChGroupExtension(node.findValue(JWTParameterKey.CHGROUP.getKey()));
        }
        if (node.findValue(JWTParameterKey.CHDELEGATION.getKey()) != null) {
            chDelegation = jsonNodeToChDelegation(node.findValue(JWTParameterKey.CHDELEGATION.getKey()));
        }
        if (iheIUAExtension == null && chEPRExtension == null && chGroupExtension == null && chDelegation == null) {
            throw new ParseException("At least one extension shall be present but none was found", 2);
        }
        return new JWTExtension(iheIUAExtension, chEPRExtension, chGroupExtension, chDelegation);
    }

    public IheIUAExtension jsonNodeToIheIUAExtension(JsonNode node) throws ParseException {
        if (node == null) {
            return null;
        }
        String subjectName = null;
        String personId = null;
        if (node.findValue(JWTParameterKey.SUBJECTNAME.getKey()) != null) {
            subjectName = node.findValue(JWTParameterKey.SUBJECTNAME.getKey()).textValue();
        }
        if (node.findValue(JWTParameterKey.PERSONID.getKey()) != null) {
            personId = node.findValue(JWTParameterKey.PERSONID.getKey()).textValue();
        }
        SubjectRoleModel subjectRole = jsonNodeToSubjectRoleModel(node.findValue(JWTParameterKey.SUBJECTROLE.getKey()));
        PurposeOfUseModel purposeOfUse = jsonNodeToPurposeOfUseModel(node.findValue(JWTParameterKey.PURPOSEOFUSE.getKey()));
        if (subjectName == null && personId == null && subjectRole == null && purposeOfUse == null) {
            throw new ParseException("At least one parameter of ihe_iua extension shall be present but none was found", 2);
        }
        return new IheIUAExtension(subjectName, personId, subjectRole, purposeOfUse);
    }

    public SubjectRoleModel jsonNodeToSubjectRoleModel(JsonNode node) throws ParseException {
        if (node == null) {
            return null;
        }
        String system = null;
        String code = null;
        if (node.findValue(JWTParameterKey.SYSTEM.getKey()) != null) {
            system = node.findValue(JWTParameterKey.SYSTEM.getKey()).textValue();
        }
        if (node.findValue(JWTParameterKey.CODE.getKey()) != null) {
            code = node.findValue(JWTParameterKey.CODE.getKey()).textValue();
        }
        if (system == null && code == null) {
            throw new ParseException("If subjectRole is present, its system and code shall be present", 1);
        }
        return new SubjectRoleModel(system, code);
    }

    public PurposeOfUseModel jsonNodeToPurposeOfUseModel(JsonNode node) throws ParseException {
        if (node == null) {
            return null;
        }
        String system = null;
        String code = null;
        if (node.findValue(JWTParameterKey.SYSTEM.getKey()) != null) {
            system = node.findValue(JWTParameterKey.SYSTEM.getKey()).textValue();
        }
        if (node.findValue(JWTParameterKey.CODE.getKey()) != null) {
            code = node.findValue(JWTParameterKey.CODE.getKey()).textValue();
        }
        if (system == null && code == null) {
            throw new ParseException("If purposeOfUse is present, its system and code shall be present", 1);
        }
        return new PurposeOfUseModel(system, code);
    }

    public ChEPRExtension jsonNodeToChEPRExtension(JsonNode node) throws ParseException {
        if (node == null) {
            return null;
        }
        String userId = null;
        String userIdQualifier = null;
        if (node.findValue(JWTParameterKey.USERID.getKey()) != null) {
            userId = node.findValue(JWTParameterKey.USERID.getKey()).textValue();
        }
        if (node.findValue(JWTParameterKey.USERIDQUALIFIER.getKey()) != null) {
            userIdQualifier = node.findValue(JWTParameterKey.USERIDQUALIFIER.getKey()).textValue();
        }
        if (userId == null || userIdQualifier == null) {
            throw new ParseException("If ch_epr extension is present, its user_id and user_id_qualifier shall be present", 1);
        }
        return new ChEPRExtension(userId, userIdQualifier);
    }

    public ChGroupExtension jsonNodeToChGroupExtension(JsonNode node) throws ParseException {
        if (node == null) {
            return null;
        }
        if (!(node instanceof ArrayNode)) {
            this.documentWellFormed.setResult("FAILED");
            XSDMessage xsdMessage = new XSDMessage();
            xsdMessage.setMessage("ch_group extension is not JSON Array");
            this.documentWellFormed.getXSDMessage().add(xsdMessage);
        }
        return new ChGroupExtension(jsonNodeToList(node));
    }

    public List<GroupModel> jsonNodeToList(JsonNode roles) throws ParseException {
        List<GroupModel> result = new ArrayList<>();
        Iterator<JsonNode> it = roles.elements();
        while (it.hasNext()) {
            result.add(jsonNodeToGroupModel(it.next()));
        }
        return result;
    }

    public GroupModel jsonNodeToGroupModel(JsonNode node) throws ParseException {
        String name = null;
        String id = null;
        if (node.findValue(JWTParameterKey.CHGROUPNAME.getKey()) != null) {
            name = node.findValue(JWTParameterKey.CHGROUPNAME.getKey()).textValue();
        }
        if (node.findValue(JWTParameterKey.CHGROUPID.getKey()) != null) {
            id = node.findValue(JWTParameterKey.CHGROUPID.getKey()).textValue();
        }
        if (name == null && id == null) {
            throw new ParseException("The group name and id shall be present for each groups in ch_group extension", 1);
        }
        return new GroupModel(name, id);
    }

    public ChDelegation jsonNodeToChDelegation(JsonNode node) throws ParseException {
        if (node == null) {
            return null;
        }
        String principal = null;
        String principalId = null;
        if (node.findValue(JWTParameterKey.PRINCIPAL.getKey()) != null) {
            principal = node.findValue(JWTParameterKey.PRINCIPAL.getKey()).textValue();
        }
        if (node.findValue(JWTParameterKey.PRINCIPALID.getKey()) != null) {
            principalId = node.findValue(JWTParameterKey.PRINCIPALID.getKey()).textValue();
        }
        if (principal == null && principalId == null) {
            throw new ParseException("If ch_delegation extension is present, its principal and principal_id shall be present", 1);
        }
        return new ChDelegation(principal, principalId);
    }

    public List<Object> getNotifications() {
        return validationResult.getWarningOrErrorOrNote();
    }

    public boolean isValidationPassed() {
        return errorCount == 0;
    }
}
