package net.ihe.gazelle.app.iti71validator.business;

import java.util.Arrays;
import java.util.List;

public enum HTTPPostHeaderParameter implements IIUARequestParameter {

    ACCEPT("Accept", ParameterType.STRING, true, "application/json"),
    CONTENTTYPE("Content-type", ParameterType.ID, true, "application/x-www-form-encoded"),
    AUTHORIZATION("Authorization", ParameterType.AUTHORIZATION, true, null);

    HTTPPostHeaderParameter(String inName, ParameterType inType, boolean isRequired, String inRegex){
        this.name = inName;
        this.type = inType;
        this.required = isRequired;
        this.regex = inRegex;
    }

    final String name;
    final String regex;
    final ParameterType type;
    boolean required;

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public ParameterType getType() {
        return this.type;
    }

    @Override
    public boolean isRequired() {
        return this.required;
    }

    @Override
    public String getRegex() {
        return this.regex;
    }

    public void setRequired(boolean required) {
        this.required = required;
    }

    public static HTTPPostHeaderParameter getParameterByName(String inName){
        for (HTTPPostHeaderParameter parameter: values()){
            if (parameter.getName().equals(inName)){
                return parameter;
            }
        }
        return null;
    }

    public static List<HTTPPostHeaderParameter> getAllParameters() {
        return Arrays.asList(values());
    }
}
