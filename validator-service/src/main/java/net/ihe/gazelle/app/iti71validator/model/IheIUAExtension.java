package net.ihe.gazelle.app.iti71validator.model;

import net.ihe.gazelle.app.iti71validator.business.JWTParameterValue;

import java.util.Map;
import java.util.TreeMap;

public class IheIUAExtension {

    private String subjectName;

    private String personId;

    private SubjectRoleModel subjectRole;

    private PurposeOfUseModel purposeOfUse;

    public IheIUAExtension(String subjectName, String personId, SubjectRoleModel subjectRole, PurposeOfUseModel purposeOfUse) {
        this.subjectName = subjectName;
        this.personId = personId;
        this.subjectRole = subjectRole;
        this.purposeOfUse = purposeOfUse;
    }

    public String getSubjectName() {
        return subjectName;
    }

    public void setSubjectName(String subjectName) {
        this.subjectName = subjectName;
    }

    public String getPersonId() {
        return personId;
    }

    public void setPersonId(String personId) {
        this.personId = personId;
    }

    public SubjectRoleModel getSubjectRole() {
        return subjectRole;
    }

    public void setSubjectRole(SubjectRoleModel subjectRole) {
        this.subjectRole = subjectRole;
    }

    public PurposeOfUseModel getPurposeOfUse() {
        return purposeOfUse;
    }

    public void setPurposeOfUse(PurposeOfUseModel purposeOfUse) {
        this.purposeOfUse = purposeOfUse;
    }

    public Map<JWTParameterValue, String> getNonNullParamAsJWTParameterValue() {
        Map<JWTParameterValue, String> allParam = new TreeMap<>();
        if (this.subjectName != null) {
            allParam.put(JWTParameterValue.SUBJECTNAME, subjectName);
        }
        if (this.personId != null) {
            allParam.put(JWTParameterValue.PERSONID, personId);
        }
        if (this.subjectRole != null) {
            allParam.putAll(subjectRole.getNonNullParamAsJWTParameterValue());
        }
        if (this.purposeOfUse != null) {
            allParam.putAll(purposeOfUse.getNonNullParamAsJWTParameterValue());
        }

        return allParam;
    }
}
