package net.ihe.gazelle.app.iti71validator.business;

import java.util.Arrays;
import java.util.List;

public enum ScopeParameter implements IIUARequestParameter {

    LAUNCH("launch", ParameterType.STRING, false, "code"),
    PURPOSEOFUSE("purpose_of_use", ParameterType.TOKEN, false, "^urn:oid:2.16.756.5.30.1.127.3.10.5\\|(NORM|EMER)$"),
    SUBJECTROLE("subject_role", ParameterType.TOKEN, false, "^urn:oid:2.16.756.5.30.1.127.3.10.6\\|(HCP|ASS|REP|PAT)$"),
    PERSONID("person_id", ParameterType.STRING, false, "^.+?\\^\\^\\^&amp;.+?&amp;ISO$"),
    PRINCIPAL("principal", ParameterType.TOKEN, false, null),
    PRINCIPALID("principal_id", ParameterType.TOKEN, false, null),
    GROUP("group", ParameterType.STRING, false, null),
    GROUPID("group_id", ParameterType.STRING, false, null),
    ACCESSTOKENFORMAT("access_token_format", ParameterType.STRING, false, "ihe-jwt|ihe-saml");

    ScopeParameter(String inName, ParameterType inType, boolean required, String inRegex){
        this.name = inName;
        this.type = inType;
        this.required = required;
        this.regex = inRegex;
    }

    final String name;
    final String regex;
    final ParameterType type;
    boolean required;

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public ParameterType getType() {
        return this.type;
    }

    @Override
    public boolean isRequired() { return this.required; }

    @Override
    public String getRegex() { return this.regex; }

    public void setRequired(boolean required) {
        this.required = required;
    }

    public static ScopeParameter getParameterByName(String inName){
        for (ScopeParameter parameter: values()){
            if (parameter.getName().equals(inName)){
                return parameter;
            }
        }
        return null;
    }

    public static List<ScopeParameter> getAllParameters() {
        return Arrays.asList(values());
    }
}
