package net.ihe.gazelle.app.iti71validator.business;

public class ValidatorConstants {

    public static final String PIXM = "PIXm";
    public static final String PDQM_RETRIEVE = "PDQm_READ";
    public static final String PDQM = "PDQm";
    public static final String XML = "XML";
    public static final String JSON = "JSON";
    public static final String OPERATION_OUTCOME = "OperationOutcome";
    public static final String FHIR = "FHIR";
    public static final String FHIR_DISCRIMINATOR = "FHIR";
    public static final String IHE_DISCRIMINATOR = "IHE";
    public static final String MCSD = "mCSD";
    public static final String MHD = "MHD";
    public static final String ATC = "ATC";
    // regex from https://www.hl7.org/fhir/datatypes.html#code
    public static final String STRING_REGEX = "[^|]+";
    public static final String CODE_REGEX = "[^\\s]+([\\s]?[^\\s]+)*";
    public static final String TOKEN_CODE_REGEX = "[^(\\s|\\|)]+([\\s]?[^(\\s\\|)]+)*";
    public static final String DATE_TIME_REGEX = "-?[0-9]{4}(-(0[1-9]|1[0-2])(-(0[0-9]|[1-2][0-9]|3[0-1])(T([01][0-9]|2[0-3]):[0-5][0-9]:[0-5][0-9]" +
            "(\\.[0-9]+)?(Z|(\\+|-)((0[0-9]|1[0-3]):[0-5][0-9]|14:00)))?)?)?";
    public static final String NUMBER_REGEX = "[-]?[1-9][0-9]*(\\.[0-9]+)?";
    public static final String TIME_REGEX = "([01][0-9]|2[0-3]):[0-5][0-9]:[0-5][0-9](\\.[0-9]+)?";
    public static final String DATE_REGEX = "-?[0-9]{4}(-(0[1-9]|1[0-2])(-(0[0-9]|[1-2][0-9]|3[0-1]))?)?";
    public static final String OID_REGEX = "urn:oid:[0-2](\\.[1-9]\\d*)+";
    public static final String ID_REGEX = "[A-Za-z0-9\\-\\.]{1,64}";
    public static final String UNSIGNED_INT_REGEX = "[0]|([1-9][0-9]*)";
    public static final String POSITIVE_INT_REGEX = "[0]|([1-9][0-9]*)";
    public static final String INTEGER_REGEX = "[0]|[-+]?[1-9][0-9]*";
    public static final String BOOLEAN_REGEX = "true|false";
    public static final String URI_REGEX = "^[a-z]([a-z0-9]|\\+|-|\\.)+:([a-zA-Z0-9]|\\+|-|\\.|:|/|@)+$";
    public static final String QUANTITY_REGEX = STRING_REGEX + "\\|" + URI_REGEX + "\\|" + CODE_REGEX;
    public static final String TOKEN_REGEX = "(" + URI_REGEX + "\\|)|(" + URI_REGEX + "\\|" + TOKEN_CODE_REGEX + ")|(\\|?" + TOKEN_CODE_REGEX + ")";
    public static final String HTTP = "http://";
    public static final String HTTPS = "https://";
    public static final String URL_PART_PATTERN = "([0-9A-Za-z]|-|_)+";
    public static final String DOMAIN = "([a-zA-Z\\d-]+(\\.)?)+(:[0-9]+)?";

    public static final String TYPE = "(Basic|Extended)";

    public static final String AUTHORIZATION = TYPE + " " + STRING_REGEX;

    private ValidatorConstants() {
        // empty
    }
}