package net.ihe.gazelle.app.iti71validator.model;

import net.ihe.gazelle.app.iti71validator.business.JWTParameterValue;

import java.util.Collections;
import java.util.Map;
import java.util.TreeMap;

public class ChEPRExtension {

    private String userId;
    private String userIdQualifier;

    public ChEPRExtension(String userId, String userIdQualifier) {
        this.userId = userId;
        this.userIdQualifier = userIdQualifier;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserIdQualifier() {
        return userIdQualifier;
    }

    public void setUserIdQualifier(String userIdQualifier) {
        this.userIdQualifier = userIdQualifier;
    }

    public Map<JWTParameterValue, String> getNonNullParamAsJWTParameterValue() {
        Map<JWTParameterValue, String> allParam = new TreeMap<>();
        if (this.userId != null && this.userIdQualifier != null) {
            allParam.put(JWTParameterValue.USERID, userId);
            allParam.put(JWTParameterValue.USERIDQUALIFIER, userIdQualifier);
            return allParam;
        }
        return Collections.emptyMap();
    }
}
