package net.ihe.gazelle.app.iti71validator.model;

import net.ihe.gazelle.app.iti71validator.business.JWTParameterValue;

import java.util.Collections;
import java.util.Map;
import java.util.TreeMap;

public class GroupModel {

    private String name;

    private String id;

    public GroupModel(String name, String id) {
        this.name = name;
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Map<JWTParameterValue, String> getNonNullParamAsJWTParameterValue() {
        Map<JWTParameterValue, String> allParam = new TreeMap<>();
        if (this.name != null && this.id != null) {
            allParam.put(JWTParameterValue.CHGROUPNAME, name);
            allParam.put(JWTParameterValue.CHGROUPID, id);
            return allParam;
        }
        return Collections.emptyMap();
    }
}
