package net.ihe.gazelle.app.validationservice.application;

import net.ihe.gazelle.app.audienceretriever.application.AudienceSecretRetriever;
import net.ihe.gazelle.modelapi.sb.business.EncoderDecoder;
import net.ihe.gazelle.sb.iua.application.IUAEncoderDecoder;

import javax.inject.Inject;

/**
 * implementation for JWTTokenValidator
 */
public class CHIUATokenValidator extends JWTTokenValidator {

    public static final String VALIDATOR_ID = "[CH:IUA]-JWT Token Validator (basic)";
    //TODO Define what is wanted in the disclaimer
    private static final String DISCLAIMER = "";

    @Inject
    private AudienceSecretRetriever audienceSecretRetriever;

    @Inject
    private IUAEncoderDecoder decoder;

    /**
     * {@inheritDoc}
     */
    public CHIUATokenValidator() {
        //Empty constructor for injection
    }

    /**
     * Setter for the audienceSecretRetriever property.
     *
     * @param audienceSecretRetriever AudienceSecretRetriever
     */
    public void setAudienceSecretRetriever(AudienceSecretRetriever audienceSecretRetriever) {
        this.audienceSecretRetriever = audienceSecretRetriever;
    }

    /**
     * setter for the JWSDecoder
     *
     * @param decoder JWSEncoderDecoder
     */
    public void setDecoder(IUAEncoderDecoder decoder) {
        this.decoder = decoder;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected AudienceSecretRetriever getAudienceSecretRetriever() {
        return this.audienceSecretRetriever;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected String getDisclaimer() {
        return DISCLAIMER;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected String getValidatorId() {
        return VALIDATOR_ID;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected EncoderDecoder getEncoderDecoder() {
        return this.decoder;
    }
}
