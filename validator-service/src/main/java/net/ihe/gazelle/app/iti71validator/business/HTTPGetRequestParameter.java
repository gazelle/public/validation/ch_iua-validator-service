package net.ihe.gazelle.app.iti71validator.business;

import java.util.Arrays;
import java.util.List;

public enum HTTPGetRequestParameter implements IIUARequestParameter {

    RESPONSETYPE("response_type", ParameterType.STRING, true, "code"),
    CLIENTID("client_id", ParameterType.ID, true, null),
    REDIRECTURI("redirect_uri", ParameterType.URI, true, null),
    STATE("state", ParameterType.STRING, true, null),
    SCOPE("scope", ParameterType.STRING, true, null),
    AUD("aud", ParameterType.STRING, true, null),
    LAUNCH("launch", ParameterType.STRING, false, null),
    CODECHALLENGE("code_challenge", ParameterType.STRING, true, null),
    CODECHALLENGEMETHOD("code_challenge_method", ParameterType.STRING, true, "S256");

    HTTPGetRequestParameter(String inName, ParameterType inType, boolean isRequired, String inRegex){
        this.name = inName;
        this.type = inType;
        this.required = isRequired;
        this.regex = inRegex;
    }

    final String name;
    final String regex;
    final ParameterType type;
    boolean required;

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public ParameterType getType() {
        return this.type;
    }

    @Override
    public String getRegex() {
        return this.regex;
    }

    @Override
    public boolean isRequired() { return this.required; }

    public void setRequired(boolean required) {
        this.required = required;
    }

    public static HTTPGetRequestParameter getParameterByName(String inName){
        for (HTTPGetRequestParameter parameter: values()){
            if (parameter.getName().equals(inName)){
                return parameter;
            }
        }
        return null;
    }

    public static List<HTTPGetRequestParameter> getAllParameters() {
        return Arrays.asList(values());
    }
}
