package net.ihe.gazelle.app.iti71validator.beans;

import net.ihe.gazelle.app.iti71validator.application.JWTExtendedTokenValidator;
import net.ihe.gazelle.app.iti71validator.application.ReportCreator;
import net.ihe.gazelle.app.iti71validator.application.RequestHTTPGetURLValidator;
import net.ihe.gazelle.app.iti71validator.application.RequestHTTPPostURLValidator;
import net.ihe.gazelle.app.validationservice.adapter.reportmodel.*;

import javax.xml.bind.annotation.*;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
        "objectToValidate",
        "url",
        "isExtended",
        "accept",
        "contentType",
        "authorization",
        "jwt"
})
@XmlRootElement(name = "chIUAObject")
public class CHIUAObject {

    @XmlElement(required = true)
    protected String objectToValidate;
    @XmlElement()
    protected String url;
    @XmlElement()
    protected Boolean isExtended;
    @XmlElement()
    protected String accept;
    @XmlElement()
    protected String contentType;
    @XmlElement()
    protected String authorization;
    @XmlElement()
    protected String jwt;

    public CHIUAObject() {
    }

    public CHIUAObject(String objectToValidate, String url, Boolean isExtended, String accept, String contentType, String authorization, String jwt) {
        this.objectToValidate = objectToValidate;
        this.url = url;
        this.isExtended = isExtended;
        this.accept = accept;
        this.contentType = contentType;
        this.authorization = authorization;
        this.jwt = jwt;
    }

    public String getObjectToValidate() {
        return objectToValidate;
    }

    public String getUrl() {
        return url;
    }

    public Boolean getExtended() {
        return isExtended;
    }

    public String getAccept() {
        return accept;
    }

    public String getContentType() {
        return contentType;
    }

    public String getAuthorization() {
        return authorization;
    }

    public String getJwt() {
        return jwt;
    }

    public DetailedResult validateObject() {
        DetailedResult result = new DetailedResult();
        MDAValidation validationResult;
        boolean status;
        switch (objectToValidate) {
            case "GET":
                RequestHTTPGetURLValidator requestHTTPGetURLValidator = new RequestHTTPGetURLValidator();
                validationResult = requestHTTPGetURLValidator.validateRequest(new IUAGetURLValidatorDescription(isExtended, url));
                result.setMDAValidation(validationResult);
                status = requestHTTPGetURLValidator.isValidationPassed();
                result.setValidationResultsOverview(ReportCreator.iuaValidationOverview(status));
                return result;

            case "POST":
                RequestHTTPPostURLValidator requestHTTPPostURLValidator = new RequestHTTPPostURLValidator();
                validationResult = requestHTTPPostURLValidator.validateRequest(new IUAPostURLValidatorDescription(url, accept, contentType, authorization));
                result.setMDAValidation(validationResult);
                status = requestHTTPPostURLValidator.isValidationPassed();
                result.setValidationResultsOverview(ReportCreator.iuaValidationOverview(status));
                return result;

            case "JWT":
                JWTExtendedTokenValidator jwtExtendedTokenValidator = new JWTExtendedTokenValidator();
                validationResult = jwtExtendedTokenValidator.validateRequest(new JWTValidatorDescription(jwt));
                result.setDocumentValidXSD(jwtExtendedTokenValidator.getDocumentValidXSD());
                result.setDocumentWellFormed(jwtExtendedTokenValidator.getDocumentWellFormed());
                result.setMDAValidation(validationResult);
                status = jwtExtendedTokenValidator.isValidationPassed();
                result.setValidationResultsOverview(ReportCreator.iuaValidationOverview(status));
                return result;
            default:
                return null;
        }
    }
}
