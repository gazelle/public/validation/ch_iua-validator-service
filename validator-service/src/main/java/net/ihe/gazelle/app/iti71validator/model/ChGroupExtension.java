package net.ihe.gazelle.app.iti71validator.model;

import net.ihe.gazelle.app.iti71validator.business.JWTParameterValue;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

public class ChGroupExtension {

    private List<GroupModel> groups;

    public ChGroupExtension(List<GroupModel> groups) {
        this.groups = groups;
    }

    public List<GroupModel> getGroups() {
        return groups;
    }

    public void setGroups(List<GroupModel> groups) {
        this.groups = groups;
    }

    public Map<JWTParameterValue, List<String>> getNonNullParamAsJWTParameterValue() {
        Map<JWTParameterValue, List<String>> allParam = new TreeMap<>();
        List<String> listName = new ArrayList<>();
        List<String> listId = new ArrayList<>();

        for (GroupModel groupModel : this.groups) {
            if (!groupModel.getNonNullParamAsJWTParameterValue().isEmpty()) {
                listName.add(groupModel.getNonNullParamAsJWTParameterValue().get(JWTParameterValue.CHGROUPNAME));
                listId.add(groupModel.getNonNullParamAsJWTParameterValue().get(JWTParameterValue.CHGROUPID));
            }
        }
        allParam.put(JWTParameterValue.CHGROUPNAME, listName);
        allParam.put(JWTParameterValue.CHGROUPID, listId);
        return allParam;
    }
}
