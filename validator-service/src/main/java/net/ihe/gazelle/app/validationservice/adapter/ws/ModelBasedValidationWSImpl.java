package net.ihe.gazelle.app.validationservice.adapter.ws;

import net.ihe.gazelle.app.validationservice.adapter.reportmodel.DetailedResult;
import net.ihe.gazelle.app.validationservice.adapter.reportmodel.MBVReportMapper;
import net.ihe.gazelle.app.validationservice.application.CHIUAExtendedTokenValidator;
import net.ihe.gazelle.app.validationservice.application.CHIUATokenValidator;
import net.ihe.gazelle.modelapi.validationreportmodel.business.ValidationReport;
import net.ihe.gazelle.validator.mb.ws.ModelBasedValidationWS;
import net.ihe.gazelle.validator.mb.ws.SOAPException_Exception;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.jws.WebService;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;

@Stateless
@WebService
public class ModelBasedValidationWSImpl implements ModelBasedValidationWS {

    private static final String INVALID_VALIDATOR = "The Validator is null or empty and should not ";
    private static final String INVALID_DOCUMENT = "The Document to validate is null or empty and should not ";

    @Inject
    private CHIUATokenValidator chiuaTokenValidator;
    @Inject
    private CHIUAExtendedTokenValidator chiuaExtendedTokenValidator;

    /**
     * Setter for the chiuaTokenValidator property.
     *
     * @param chiuaTokenValidator value to set to the property.
     */
    public void setCHIUATokenValidator(CHIUATokenValidator chiuaTokenValidator) {
        this.chiuaTokenValidator = chiuaTokenValidator;
    }

    /**
     * Setter for the chiuaExtendedTokenValidator property.
     *
     * @param chiuaExtendedTokenValidator value to set to the property.
     */
    public void setChiuaExtendedTokenValidator(CHIUAExtendedTokenValidator chiuaExtendedTokenValidator) {
        this.chiuaExtendedTokenValidator = chiuaExtendedTokenValidator;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String validateBase64Document(String base64Document, String validator) throws SOAPException_Exception {
        if (base64Document == null || base64Document.isEmpty()) {
            throw new SOAPException_Exception(INVALID_DOCUMENT);
        }
        byte[] documentBytes = Base64.getDecoder().decode(base64Document);
        if (documentBytes != null) {
            String document = new String(documentBytes, StandardCharsets.UTF_8);
            return this.validateDocument(document, validator);
        } else {
            return this.validateDocument(null, validator);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String validateDocument(String document, String validator) throws SOAPException_Exception {
        if (validator == null || validator.isEmpty()) {
            throw new SOAPException_Exception(INVALID_VALIDATOR);
        }
        if (document == null || document.isEmpty()) {
            throw new SOAPException_Exception(INVALID_DOCUMENT);
        }
        ValidationReport validationReport;
        if (validator.equals(CHIUATokenValidator.VALIDATOR_ID)) {
            validationReport = chiuaTokenValidator.validateToken(document);
        } else if (validator.equals(CHIUAExtendedTokenValidator.VALIDATOR_ID)) {
            validationReport = chiuaExtendedTokenValidator.validateToken(document);
        } else {
            throw new SOAPException_Exception(String.format("Unknown validator %s", validator));
        }

        DetailedResult detailedResult = MBVReportMapper.map(validationReport);

        return MBVReportMapper.getDetailedResultAsString(detailedResult);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String about() {
        StringBuilder disclaimer = new StringBuilder();
        disclaimer.append("Gazelle test bed about validator");
        return disclaimer.toString();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<String> getListOfValidators(String descriminator) {
        List<String> validatorList = new ArrayList<>();
        validatorList.add(CHIUATokenValidator.VALIDATOR_ID);
        validatorList.add(CHIUAExtendedTokenValidator.VALIDATOR_ID);
        return validatorList;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getValidatorsAsString() {
        throw new UnsupportedOperationException();
    }
}
