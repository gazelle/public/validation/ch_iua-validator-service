package net.ihe.gazelle.app.iti71validator.model;

import net.ihe.gazelle.app.iti71validator.business.JWTParameterValue;

import java.util.Map;
import java.util.TreeMap;

public class JWTExtension {

    private IheIUAExtension iheIUAExtension;
    private ChEPRExtension chEPRExtension;
    private ChGroupExtension chGroupExtension;
    private ChDelegation chDelegation;

    public JWTExtension() {
    }

    public JWTExtension(IheIUAExtension iheIUAExtension, ChEPRExtension chEPRExtension, ChGroupExtension chGroupExtension, ChDelegation chDelegation) {
        this.iheIUAExtension = iheIUAExtension;
        this.chEPRExtension = chEPRExtension;
        this.chGroupExtension = chGroupExtension;
        this.chDelegation = chDelegation;
    }

    public IheIUAExtension getIheIUAExtension() {
        return iheIUAExtension;
    }

    public void setIheIUAExtension(IheIUAExtension iheIUAExtension) {
        this.iheIUAExtension = iheIUAExtension;
    }

    public ChEPRExtension getChEPRExtension() {
        return chEPRExtension;
    }

    public void setChEPRExtension(ChEPRExtension chEPRExtension) {
        this.chEPRExtension = chEPRExtension;
    }

    public ChGroupExtension getChGroupExtension() {
        return chGroupExtension;
    }

    public void setChGroupExtension(ChGroupExtension chGroupExtension) {
        this.chGroupExtension = chGroupExtension;
    }

    public ChDelegation getChDelegation() {
        return chDelegation;
    }

    public void setChDelegation(ChDelegation chDelegation) {
        this.chDelegation = chDelegation;
    }

    public Map<JWTParameterValue, Object> getNonNullParamAsJWTParameterValue() {
        Map<JWTParameterValue, Object> allParam = new TreeMap<>();
        if (this.iheIUAExtension != null) {
            allParam.putAll(iheIUAExtension.getNonNullParamAsJWTParameterValue());
        }
        if (this.chEPRExtension != null) {
            allParam.putAll(chEPRExtension.getNonNullParamAsJWTParameterValue());
        }
        if (this.chGroupExtension != null) {
            allParam.putAll(chGroupExtension.getNonNullParamAsJWTParameterValue());
        }
        if (this.chDelegation != null) {
            allParam.putAll(chDelegation.getNonNullParamAsJWTParameterValue());
        }

        return allParam;
    }
}
