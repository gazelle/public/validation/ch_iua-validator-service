package net.ihe.gazelle.app.iti71validator.ws;

import net.ihe.gazelle.app.iti71validator.application.ReportCreator;
import net.ihe.gazelle.app.iti71validator.beans.CHIUAObject;

import javax.ejb.Stateless;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.xml.ws.RequestWrapper;
import javax.xml.ws.ResponseWrapper;

@Stateless
@WebService(name = "ITI71ValidatorWS", targetNamespace = "http://ws.iti71validator.app.gazelle.ihe.net")
public class ITI71ValidatorWSImpl implements ITI71ValidatorWS {

    @WebMethod
    @RequestWrapper(localName = "queryValidator", targetNamespace = "http://ws.iti71validator.app.gazelle.ihe.net", className = "net.ihe.gazelle.app.iti71validator.ws.QueryValidator")
    @ResponseWrapper(localName = "queryValidatorResponse", targetNamespace = "http://ws.iti71validator.app.gazelle.ihe.net", className = "net.ihe.gazelle.app.iti71validator.ws.QueryValidatorResponse")
    @WebResult(name = "DetailedResult", targetNamespace = "")
    public String queryValidator(@WebParam(name = "objectToValidate", targetNamespace = "") String objectToValidate,
                                 @WebParam(name = "url", targetNamespace = "") String url,
                                 @WebParam(name = "isExtended", targetNamespace = "") Boolean isExtended,
                                 @WebParam(name = "accept", targetNamespace = "") String accept,
                                 @WebParam(name = "contentType", targetNamespace = "") String contentType,
                                 @WebParam(name = "authorization", targetNamespace = "") String authorization,
                                 @WebParam(name = "jwt", targetNamespace = "") String jwt) {
        CHIUAObject chiuaObject = new CHIUAObject(objectToValidate, url, isExtended, accept, contentType, authorization, jwt);
        return ReportCreator.getDetailedResultAsString(chiuaObject.validateObject());
    }
}
