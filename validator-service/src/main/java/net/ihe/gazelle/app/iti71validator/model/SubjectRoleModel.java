package net.ihe.gazelle.app.iti71validator.model;

import net.ihe.gazelle.app.iti71validator.business.JWTParameterValue;

import java.util.Collections;
import java.util.Map;
import java.util.TreeMap;

public class SubjectRoleModel {
    private String system;
    private String code;

    public SubjectRoleModel(String system, String code) {
        this.system = system;
        this.code = code;
    }

    public String getSystem() {
        return system;
    }

    public void setSystem(String system) {
        this.system = system;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Map<JWTParameterValue, String> getNonNullParamAsJWTParameterValue() {
        Map<JWTParameterValue, String> allParam = new TreeMap<>();
        if (this.system != null && this.code != null) {
            allParam.put(JWTParameterValue.SUBJECTROLESYSTEM, system);
            allParam.put(JWTParameterValue.SUBJECTROLECODE, code);
            return allParam;
        }
        return Collections.emptyMap();
    }
}
