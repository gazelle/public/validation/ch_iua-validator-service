package net.ihe.gazelle.app.iti71validator.application;

import net.ihe.gazelle.app.validationservice.adapter.reportmodel.*;
import net.ihe.gazelle.app.validationservice.adapter.reportmodel.Error;
import net.ihe.version.ws.Interface.Preferences;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class ReportCreator {
    public static final String PASSED = "PASSED";
    public static final String FAILED = "FAILED";
    private static final Logger LOGGER = LoggerFactory.getLogger(ReportCreator.class);
    public static final String ABORTED = "ABORTED";
    public static final String SKIPPED = "SKIPPED";

    private ReportCreator() {
    }

    public static ValidationResultsOverview iuaValidationOverview(boolean passed) {
        ValidationResultsOverview validationResultsOverview = new ValidationResultsOverview();
        Date currentDate = new Date();
        DateFormat dateFormat = new SimpleDateFormat("yyyy, MM dd");
        DateFormat timeFormat = new SimpleDateFormat("hh:mm:ss");
        validationResultsOverview.setValidationDate(dateFormat.format(currentDate));
        validationResultsOverview.setValidationTime(timeFormat.format(currentDate));
        validationResultsOverview.setValidationServiceName("Gazelle CH:IUA ITI-71 Validator");
        validationResultsOverview.setValidationEngine("iua");
        validationResultsOverview.setValidationServiceVersion(Preferences.getProperty("buildVersion"));

        String status = passed ? PASSED:FAILED;
        validationResultsOverview.setValidationTestResult(status);
        return validationResultsOverview;
    }

    public static String getDetailedResultAsString(DetailedResult detailedResult) {
        if (detailedResult != null) {
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            try {
                JAXBContext jc = JAXBContext.newInstance("net.ihe.gazelle.app.validationservice.adapter.reportmodel");
                Marshaller m = jc.createMarshaller();
                m.setProperty(Marshaller.JAXB_ENCODING, StandardCharsets.UTF_8.name());
                m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
                m.marshal(detailedResult, baos);
            } catch (JAXBException e) {
                LOGGER.error(e.getMessage(), e);
            }
            try {
                return baos.toString(StandardCharsets.UTF_8.name());
            } catch (UnsupportedEncodingException e) {
                LOGGER.warn("UTF-8 encoding is not supported, relying on default encoding: " + Charset.defaultCharset().displayName(), e);
                return null;
            }
        }
        return null;
    }

    public static DetailedResult buildReportForAbortedValidation(IOException e) {
        DetailedResult detailedResult = new DetailedResult();
        ValidationResultsOverview overview = iuaValidationOverview(false);
        overview.setValidationTestResult(ABORTED);
        detailedResult.setValidationResultsOverview(overview);
        MDAValidation mdaValidation = new MDAValidation();
        mdaValidation.setResult(SKIPPED);
        Notification notification = new Error();
        notification.setLocation("message");
        notification.setDescription(e.getMessage());
        notification.setIdentifiant("Pre-processing step");
        mdaValidation.getWarningOrErrorOrNote().add(notification);
        detailedResult.setMDAValidation(mdaValidation);
        return detailedResult;
    }
}
