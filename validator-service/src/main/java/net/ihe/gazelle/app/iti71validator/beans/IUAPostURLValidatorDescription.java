package net.ihe.gazelle.app.iti71validator.beans;

import net.ihe.gazelle.app.iti71validator.business.HTTPPostHeaderParameter;
import net.ihe.gazelle.app.iti71validator.business.HTTPPostRequestParameter;

import java.util.List;
import java.util.Objects;

//TODO à remanier lors du refacto
public class IUAPostURLValidatorDescription {

    private List<HTTPPostHeaderParameter> headerParameters;

    private List<HTTPPostRequestParameter> requestParameters;

    private final String accept;

    private final String contentType;

    private final String authorization;

    private final String urlToValidate;

    public IUAPostURLValidatorDescription(String utlToValidate, String accept, String contentType, String authorization) {
        this.headerParameters = HTTPPostHeaderParameter.getAllParameters();
        this.requestParameters = HTTPPostRequestParameter.getAllParameters();
        this.accept = accept;
        this.contentType = contentType;
        this.authorization = authorization;
        this.urlToValidate = utlToValidate;
    }

    public List<HTTPPostHeaderParameter> getHeaderParameters() {
        return headerParameters;
    }

    public void setHeaderParameters(List<HTTPPostHeaderParameter> headerParameters) {
        this.headerParameters = headerParameters;
    }

    public List<HTTPPostRequestParameter> getRequestParameters() {
        return requestParameters;
    }

    public void setRequestParameters(List<HTTPPostRequestParameter> requestParameters) {
        this.requestParameters = requestParameters;
    }

    public String getAccept() {
        return accept;
    }

    public String getContentType() {
        return contentType;
    }

    public String getAuthorization() {
        return authorization;
    }

    public String getResourceName() {
        return "token";
    }

    public String getUrlToValidate() {
        return urlToValidate;
    }

    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof IUAPostURLValidatorDescription)) {
            return false;
        }

        IUAPostURLValidatorDescription that = (IUAPostURLValidatorDescription) o;
        return Objects.equals(headerParameters, that.headerParameters) && Objects.equals(requestParameters, that.requestParameters);
    }

    public int hashCode() {
        int result = headerParameters != null ? headerParameters.hashCode() : 0;
        result = 31 * result + (requestParameters != null ? requestParameters.hashCode() : 0);
        return result;
    }
}
