package net.ihe.gazelle.app.iti71validator.beans;

import java.util.Objects;

public class JWTValidatorDescription {

    private String jwt;

    public JWTValidatorDescription(String jwt) {
        this.jwt = jwt;
    }

    public String getJwt() {
        return jwt;
    }

    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof JWTValidatorDescription)) {
            return false;
        }

        JWTValidatorDescription that = (JWTValidatorDescription) o;
        return Objects.equals(jwt, that.jwt);
    }

    public int hashCode() {
        return jwt != null ? jwt.hashCode() : 0;
    }
}
